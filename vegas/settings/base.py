"""Base Settings Module

You shouldn't need to update or change this file! All your changes should go
in the local_settings.py file."""
import os
import re

import aioredis
import aiopg


async def setup_redis(bot):
    """Provides a basic connection to redis.
    By default this will connect to Redis on the provided Docker-Compose file"""
    bot.redis = await aioredis.create_redis_pool("redis://redis")


async def setup_postgres_pool(bot):
    """Provides a basic connection to Postgres.
    By default this will connect to Postgres on the provided Docker-Compose file"""
    bot.pg_pool = await aiopg.create_pool(POSTGRES_AUTH)


DEBUG = False

PG_HOST = os.environ.get("DB_HOST")
PG_NAME = os.environ.get("DB_NAME")
PG_USER = os.environ.get("DB_USER")
PG_PASSWORD = os.environ.get("DB_PASS")

POSTGRES_AUTH = (
    f"dbname={PG_NAME} user={PG_USER} password={PG_PASSWORD} port=5432 host={PG_HOST}"
)

# Prefix used for bot commands
BOT_PREFIX = ("vt?", "vt!")

COLORS = {
    "ban": 7821292,  # Snap Purple
    "unban": 13429847,
    "edit": 15538157,
    "delete": 14487859,
    "voice": 1904757,
    "role_removed": 16759569,
    "role_added": 4513245,
    "join": 15663086,
    "left": 1114129,
    "profile_change": 14318337,
    "username": 14318337,
    "avatar": 14318337,
}

# What plugins to load from the `plugins` directory
COGS = (
    'plugins.notifications',
    'plugins.among_us',
    'plugins.embed',
    'plugins.qotd',
    'plugins.games',
    'plugins.fun',
    "plugins.loaders",
    "plugins.messages",
    "plugins.utilities"
)

# The number of messages discord.py stores in memory
MAX_MESSAGES = 5000


NOTIFICATION_DICT = {
    "Game Theory Notification": "<:GameTheory:695120563991216178>",
    "Film Theory Notification": "<:FilmTheory:695120565081735208>",
    "Food Theory Notification": "<:FoodTheory:735996033711997029>",
    "GTLive Notification": "<:GTLive:695120565236662312>",
    "GTARG Notification": "<:tenretni:776452483823304745>",
    "Theory Event Notification": "<:TheoryTheory:735924393397583995>",
    "Art Event Notification": "<U+1F3A8>",
    "Writing Event Notification": "✍️",
    "Music Event Notification": "<U+1F3B5>",
    "Meme Event Notification": "<:SansIsNess:695120564318109816>",
    "Question of the Day Notification": "<:QuestionBlock:695120559377350696>",
    "Community Events Notification": "<:GTStamp:816037150952259594>",  # emoji needs updaed
    "Cooking Event Notification": "<U+1F9D1><U+200D><U+1F373>",
    "Books and More Notification": "<:BookTheory:721427272510210059>",
    "Resolution Checkup Notification": "<:SweatPat:695120565253439490>",
    "Approved/Staff Notification": "<:emodseal:695120556046942238>",
    "Volunteer Notification": "<:SuperStar:695120560258285690>",
    "Community ARG Notification": "<:sus_think:695120561352736870>",
    "Stream Notification": "<:kiwipopcorn:695120561902321694>",
    "VC Music Notification": "<U+1F3A4>",
    "Among Us Game Notification": "<:AmongPat:759778089298690099>",
    "Duck Game Switch Notification": "<U+1F986>",
    "professor asters coding 101 Notification": "<:CoolAsterWithFingerGunsWowww:695120559612231771>",
    "InfoDump Notification": "<U+1F4E3>",
    "Ask Pronouns": "<U+1F4AC>",
    "He/Him": "♂️",
    "She/Her": "♀️",
    "They/Them": "⚧",
}

INTERESTS_DICT = {
    "Game Theory": "Obviously, as the official Game Theorists Server, we have an entire category dedicated to their channels! For general discussion, charity or other special streams and appearances, merch, Theorist news, etc., head to <#783103400317026354>!\n\nIf you want to discuss a video, try these channels:\n<#353694243346186240>\n<#354759120131325963>\n<#735157933440106578>\n<#353694504634810378>\n\nThe latest videos will be posted in their respective channels and in <#483371293496836096>.\n\nIf you want to see official social media posts, you can go to <#689583295612190815>--you can discuss these posts in <#783103400317026354>!",
    "Anime": "If you want to talk about Anime, head over to <#438161754728562688>--just make sure it’s a server appropriate anime before discussing. Be sure to use proper spoiler tags for any spoilers! If you want to theorize on an anime, we have <#714295314202361886>, run by our Approved Theorizers! You can ping the Theorizing Critic role if you need any help with your theories.",
    "ARGs": "Since the GTARG is how our server gained a lot of its members, ARGs are very popular in this server. This includes the ones the Game Theorists host, those found on the internet, and the ones made by our own community. For a GTARG, head to <#490956524571721739>. For anything else ARG (asking for help with one or making one, sharing one you made or found, etc.), try <#519949835483217940>.",
    "Art": "We are happy to have an incredibly active art community in our server! <#354405657484460043> is the channel for all our artists, where members of any level can post their art or post art of others with proper credits. The channel is run by our Approved Artists, and you can ping the Art Critic role for help. Check the pins for any ongoing events and be sure to follow the channel and server rules!",
    "Avatars": "Picrew links and Personality tests (like the Myer-Briggs Test) became so popular that we added a channel for them! Head over to <#804470795132600320> to share or use picrews and tests with your fellow Theorists.",
    "Books": "Though Book Theory may not be a thing (yet), we have many avid readers in our community. Books, Poetry, Web Comics, Manga, Comics, Graphic Novels, and anything else in between are all welcome in <#553459376908795905>!",
    "Bots": "Bots are an important part of our server. For any bot besides Groovy, use <#377304867686842368> for most of the commands. If you want to use Groovy, <#558331356170420224> and Groovy Room VC are the place to be. If you want to learn how to program a bot, head on over to <#528768776892579840> and check the pins--that’s also where you can find any programming events we host!",
    "Cooking": "If food is your forte, you’ll love <#528253600682737685>! Whether you want to share something you made, ask for cooking advice, or just want to talk about food, this channel is the place to be.",
    "Debate": "Our Serious Channels category contains <#796488552262795285> and <#428919368810889219> for respectful and informed debate and discussion of current events, politics, and other similar subjects. Be sure to review the channel rules in the pins and the server rules before joining in any debate.",
    "FNaF": "Five Nights at Freddy’s is an incredibly popular topic with our theorizing community! If you want to talk about old Game Theories about FNaF, try <#353694243346186240>. For actually playing the games, head over to <#435437770110926848>. For the brave theorists among you, <#559469991473315840> is the channel dedicated to discussion and theorizing about all the official FNaF games. We have Approved Theorizers who run this channel, and you can ping the Theorizing Critic role for advice!",
    "Gaming": "We wouldn't be the Game Theorists server without lots of places for gaming! If you want to talk about what games you’re playing or the latest news in gaming, try <#435437770110926848>. We also have <#414869900583239683> for playing games with your fellow theorists (especially Among Us). The Gaming Room VC channel is the best place to play games and talk with your fellow theorists--those theorists who have reached a certain level and applied for the needed roles can stream in Streaming Room VC. Keep an eye out for any official streams, especially Jackbox games!",
    "LGBTQ+": "We have an incredibly diverse community and love to make this server an accepting place for everyone. <#658807627576246272> serves as our non-gendered safe space for talking about gender, sexuality, and all other LGBTQ+ issues (though almost any other Serious Channel can be used for this too). Keep an eye out for the events and server changes we make for Pride Month!",
    "Memes": "This wouldn't be the internet without a place for memes, now would it? We have <#353766146115371009> for all your meme-making needs--our Expert Meme-ers are dedicated to keeping the channel a fun and appropriate space. You need at least New Theorist to post in this channel. If you’re lucky, your meme might be pinned and posted in <#580916681849962519>. Please make sure to check the server and channel rules before posting!\n\nAdditionally, you can post memes on the [Game Theorists Subreddit](https://www.reddit.com/r/GameTheorists/) on Mondays for a chance to be featured on the ever-popular Meme Review, or our [meme subreddit](https://www.reddit.com/r/gametheorymemes/) every single day of the week!",
    "Minecraft": "Minecraft is another incredibly popular game for the channel. If you want to talk about old Game Theories about Minecraft, try <#353694243346186240>. For showing off your builds and talking about your Minecraft exploits, head on over to <#435437770110926848>. You can also find the IP for the server’s official Java Minecraft server in the pins--make sure to read the instructions on how to be whitelisted. For your theorizing and Minecraft news needs, you can use <#604346935029268480>. We have a team of Approved Theorizers running the channel, and you can ping the Theorizing Critic role for help with your theories!",
    "Movies": "If you love movies or TV shows, we’ve got plenty of places for you! To talk about old film theory videos, head on over to <#354759120131325963>! If you want to make your own theories, we have <#714295314202361886>, run by our Approved Theorizers! You can ping the Theorizing Critic role if you need any help with your theories. Finally, be sure to watch <#702712354491859086> for our movie nights!",
    "Music": "If you’re musically inclined (or just like listening to and talking about music), we’ve got plenty of places for you! <#497917931716476928> is our main hub for everything music related--you can post links to your favorite songs or share your own music. Just be sure to read the channel rules! The channel is moderated by our Approved Musicians, and you can ping the Music Critic role if you want feedback on your own creations. If you want to listen to music with your fellow server members, you can use Groovy Room VC (just make sure to do the commands in <#558331356170420224>--rules in the pins)! Finally, we’ve got the Music Room VC for you to perform or listen to your fellow Theorists!",
    "Nintendo": "You can’t have Game Theory without Nintendo! If you want to talk about old Game Theories about Nintendo, try <#353694243346186240>. We also have sheets to put friend codes in for various nintendo games in <#435437770110926848>. If you wanna theorize or talk about the latest news from Nintendo, head on over to <#436286653582016554>! We have a team of Approved Theorizers running the channel, and you can ping the Theorizing Critic role for help with your theories!",
    "Puzzles": "ARGs a little much for you? You can still head over to <#519949835483217940> to enjoy puzzles made by our community or to make some of your own. Keep an eye on <#702712354491859086> for our occasional puzzle challenges!",
    "Reddit": "If you’re here, chances are you found this server via the official subreddit--if you didn't, we have one! Head on over to the [Game Theorists Subreddit](https://www.reddit.com/r/GameTheorists/) to post your theories any day of the week, Memes on Mondays, and Fan Art on Fridays. If Meme Mondays aren't enough for you, we have the [Game Theory Memes Subreddit](https://www.reddit.com/r/gametheorymemes/) for meme posting any day of the week!",
    "Roles": "Like most servers, we have a variety of roles. For Staff roles or leveled roles, check out <#735635802960429087> for more information! We have Critic and Streamer roles available by application via the form in <#672593109892464640>. We have roles for request in <#560538258115919872>--check the pins for the requestable roles and their requirements! Finally, if you want to get pronoun roles or notification roles, head on over to <#377304867686842368>. There’s a pinned guide on how to get them and what the available roles are.",
    "Server Events": "We love hosting events in this server--and our staff do a great job of hosting them! Here are a list of channels that often have events listed in their pins:\n<#354405657484460043>\n<#528253600682737685>\n<#353766146115371009>\n<#497917931716476928>\n<#528768776892579840>\n<#386969766343999488>\n<#353694144775847960>\n\nWe also have <#702712354491859086>, where all our events are announced!",
    "Social Issues": "We strive to make the server a safe space to talk about social issues, which is one of the goals of our Serious Channels category. <#499083430009765889>, <#499097670883868675>, and <#658807627576246272> are the best channels for gender and LGBTQ+ social issues. <#796488552262795285> is the best channel for recent political news relating to social issues, and <#428919368810889219> and <#465920060872065024> cover everything else.",
    "Socializing": "This is an incredibly welcoming community! If you haven’t yet introduced yourself, you should head over to <#394732370282151936>! After that, you can head over to <#353694095715205121> or <#368179610376077314> to start chatting. If VC is more your thing, we have Off Topic VC and <#502050241126596618>.",
    "STEM": "STEM is a huge part of the Game Theorists Discord Server! <#528768776892579840> is the hub for all our STEM discussion--check the pins for coding challenges and a link to the bot testing server, where you can help code V.E.G.A.S. and learn to make your own bot!",
    "Theorizing": "Just like the subreddit, our server has an active theorizing base, which is housed in our Theorizing and Discussion category! If the topic you want to theorize isn’t covered under one of the other theorizing channels, head to <#353694144775847960>--remember to read the channel rules! We have a team of Approved Theorizers making sure the channels are running smoothly. If you want feedback on a theory, you can ping the Theorizing Critic role! If you complete a theory, you can submit it to the <#591647011476996106>--check the pins for how to submit.",
    "Venting": "Our community is very supportive when it comes to venting. You can use <#465920060872065024> for general venting, or <#428919368810889219> if <#465920060872065024> is occupied. <#499083430009765889>, <#499097670883868675>, and <#658807627576246272> can also be used for more specific venting. Please remember to read and follow all the channel rules before using these channels.",
    "Writing": "If you love writing, then you’ll definitely want to check out <#386969766343999488>! Our Approved Writers keep the channel running smoothly, and you can ping Writing Critic if you want feedback. Please only post original content, and remember to read and follow the channel rules before posting.",
}

INTERESTS_DICT1 = {
    "Game Theory": ["Film Theory", "Food Theory", "GTLive", "MatPat"],
    "ARGs": ["GTARG"],
    "Art": ["Photography", "Sculpting", "Painting"],
    "Avatars": ["Personality Tests", "Astrology", "Pfp Makers", "Picrew"],
    "Books": ["Reading", "Comics", "Manga"],
    "Cooking": ["Food", "Baking"],
    "Debate": ["Politics", "News"],
    "FNaF": ["Five Nights At Freddy's", "Scott Cawthon"],
    "Gaming": ["Among Us", "Video Games"],
    "LGBTQ+": ["LGBT", "LGBTQ", "LGBT Issues"],
    "Movies": ["Film", "Cinema", "TV", "TV Shows"],
    "Music": ["Instruments", "Composing", "Singing"],
    "Nintendo": ["Mario", "SSBU", "Splatoon", "Pokemon", "Animal Crossing"],
    "Reddit": ["Subreddit", "GT Subreddit", "GT Reddit"],
    "Roles": ["Notifications"],
    "Server Events": ["Community Events", "Competitions", "Challenges", "Events"],
    "Socializing": ["Chatting", "Making Friends"],
    "STEM": ["Technology", "Math", "Science", "Programming"],
    "Theorizing": ["Theories", "Lore"],
    "Venting": ["Vent", "Emotional Support"],
    "Writing": ["Poetry"],
}

RULES_TRANSFER = {
    "1": "Rule 1. Be Nice, Be Respectful",
    "2": "Rule 2. No spamming",
    "3": "Rule 3. No shitposting",
    "4": "Rule 4. No NSFW content",
    "5": "Rule 5. Sensitive Topics",
    "6": "Rule 6. No self-promotion without consent of a moderator",
    "7": "Rule 7. No penalty evasion",
    "8": "Rule 8. No brigading",
    "9": "Rule 9. Keep channels on topic",
    "10": "Rule 10. No impersonation",
}

RULES_BODY = {
    "Rule 1. Be Nice, Be Respectful": "    This should go without saying, but be respectful. Discussion and debate are allowed (in the appropriate channels) as long as it remains civil!\n    No sexism, racism, bigotry, homophobia, transphobia, slurs, etc.",
    "Rule 2. No spamming": "    Unreasonably direct messaging or pinging members or being disruptive in any chat is not allowed. Additionally, please never ping the GT Cast. If you need help or have a question related to the server, you can ask in <#516027780908056578>.\n    In general, any unnecessary pings anywhere should be avoided.",
    "Rule 3. No shitposting": "    'Shitposting' describes any messages that disrupt a conversation or channel with humorous intent. We have a channel dedicated to this, <#353766146115371009>, so shitposts are not allowed elsewhere. Additionally, spam is prohibited in that channel as well.\n    Copypastas and Insensitive/Otherwise rule-breaking shitposting are not permitted anywhere. The same goes for posting an unreasonable amount of emotes/only using emotes instead of text, etc.",
    "Rule 4. No NSFW content": "    As a rule of thumb, the general content in this server should be around PG-13. Sharing any messages or content that would be unsuited for younger members is not allowed. This includes pictures, links, videos, slurs, excessive swearing, abusing emoji/reactions, and gore. There is a banned word list pinned in #server-help.\n    This may result in an immediate Ban.\n    This rule also includes NSFW (or otherwise rule-breaking) Profile Pictures, Custom Statuses, and Usernames.",
    "Rule 5. Sensitive Topics": "    There is a Serious Topics category where you can discuss serious topics or vent. However, make sure to spoiler potentially triggering content and add a warning.\n    Please note that while discussion of sensitive topics is allowed, glorification or encouragement of unhealthy, dangerous, and/or harmful behavior is strictly forbidden and warrants a ban.\n    Insensitivity towards serious topics, especially when it’s about another person, is not tolerated and includes everything from Rule 1.",
    "Rule 6. No self-promotion without consent of a moderator": "    DMing people about your content without them specifically asking you to, also counts as self-promo. This includes asking people to join Discord Servers and sharing your own videos.\n    Linking to your profile from anywhere is not allowed. Do not share content that you are in any way involved with or related to. Includes crowdsourcing pages like gofundme.\n    Calls to action such as “check out my channel”, “leave a like”, etc are also self-promo.\n    Only links to trusted/popular websites such as Youtube, Soundcloud, Twitter, etc. are allowed. Additionally, make sure to check the channel pins to see what is allowed and what is not.\n    If you believe to have an important link that would fall under the self-promo rules, you can DM a mod and ask for approval to post it.",
    "Rule 7. No penalty evasion": "    Using multiple accounts or leaving and rejoining the server to attempt to get around penalties is not allowed under any circumstances. You are allowed to use multiple accounts on the server, but using them to avoid punishments (or double potential rewards) is not allowed.",
    "Rule 8. No brigading": "    Brigading/Raiding other subreddits or other Discord servers is breaking TOS. Any users caught brigading will be banned and reported.",
    "Rule 9. Keep channels on topic": "    There are a lot of channels with specific topics, so if you want to discuss a certain topic please move to the appropriate channel. If you are unsure what a channel is for you can click on it and read the channel topic at the top (on PC) or on the sidebar (on mobile). If you are still unsure you can ask in #server-help or DM a Mod or Disaster Director.\n    Failure to follow when a Staff member asks you to move to a channel will result in a warn/mute.",
    "Rule 10. No impersonation": "    Pretending to be another person without their consent is forbidden. This includes Game Theory members like MatPat, other celebrities, discord employees, and staff members of this server.",
}

ROLE_PERMS = [
    {
        "name": "Divine Theorist",
        "perms": [
            {
                "desc": "•**Prestige?** You can't prestige until you reach <@&514115149158678550>. \n",
                "tags": [],
            }
        ],
    },
    {"name": "Ascended Theorist", "perms": []},
    {
        "name": "Sage Theorist",
        "perms": [
            {
                "desc": "•**Use !degendex?** You can't use !degendex until you reach <@&514115324841426954>. \n",
                "tags": [],
            }
        ],
    },
    {"name": "Pure Theorist", "perms": []},
    {
        "name": "Legendary Theorist",
        "perms": [
            {
                "desc": "•**Go Live?** You can't Go Live in a VC until you reach <@&514115410396839955>. \n",
                "tags": [],
            }
        ],
    },
    {
        "name": "Limitless Theorist",
        "perms": [
            {
                "desc": "•**Post in warn reports?** You can't post in <#624257664125501450> until you reach <@&514115613468393473>. \n",
                "tags": [],
            }
        ],
    },
    {"name": "Inhuman Theorist", "perms": []},
    {
        "name": "Master Theorist",
        "perms": [
            {
                "desc": "•**Use !sleeping-chat?** You can't use !sleeping-chat until you reach <@&514115743151816714>. \n",
                "tags": [],
            }
        ],
    },
    {"name": "Genius Theorist", "perms": []},
    {"name": "Enlightened Theorist", "perms": []},
    {
        "name": "Insane Theorist",
        "perms": [
            {
                "desc": "•**See the mod preview channel?** You can't see <#519873222506709002> until you reach <@&514116251723890732>. \n",
                "tags": [],
            }
        ],
    },
    {
        "name": "Obsessed Theorist",
        "perms": [
            {
                "desc": "•**Post in daily positivity?** You can't post in <#565566736800284683> until you reach <@&514116287283331073>. \n",
                "tags": [],
            }
        ],
    },
    {"name": "Addicted Theorist", "perms": []},
    {
        "name": "Dedicated Theorist",
        "perms": [
            {
                "desc": "•**Be recommended for Approved roles?** You can't be recommended for Approved roles until you reach <@&514116355763732480>. \n",
                "tags": [],
            }
        ],
    },
    {
        "name": "Loyal Theorist",
        "perms": [
            {
                "desc": "•**Send suggestions?** You can't suggest in the Suggestions channels until you reach <@&514116391607992332>. \n",
                "tags": ["post"],
            }
        ],
    },
    {
        "name": "Serious Theorist",
        "perms": [
            {
                "desc": "•**Change my nickname?** You can't change your nickname until you reach <@&514114232011194379>. \n",
                "tags": [],
            }
        ],
    },
    {
        "name": "Hard-Working Theorist",
        "perms": [
            {
                "desc": "•**Attach files?** You can't attach files (outside of <#354405657484460043>, <#528253600682737685>, <#353766146115371009> and the Theorizing channels) until you reach <@&514114175409061900>. \n",
                "tags": [],
            },
            {
                "desc": "•**Embed links?** You can't embed links (outside of <#354405657484460043>, <#528253600682737685>, <#353766146115371009> and the Theorizing channels) until you reach <@&514114175409061900>. \n",
                "tags": [],
            },
            {
                "desc": "•**See suggestion channels?** You can't see the Suggestions channels until you reach <@&514114175409061900>. \n",
                "tags": [],
            },
        ],
    },
    {
        "name": "Improving Theorist",
        "perms": [
            {
                "desc": "•**Use emojis from other servers?** You can't use external emoji until you reach <@&514114132392411157>. \n",
                "tags": ["emote"],
            },
            {
                "desc": "•**Use !yeet?** You can't use !yeet until you reach <@&514114132392411157>. \n",
                "tags": [],
            },
        ],
    },
    {
        "name": "Rookie Theorist",
        "perms": [
            {
                "desc": "•**Add reactions?** You can't add reactions until you reach <@&514114064608264205>. \n",
                "tags": [],
            },
            {
                "desc": "•**Send images?** You can only send images in the Theorizing Channels once you reach <@&514114064608264205>. \n",
                "tags": [],
            },
        ],
    },
    {
        "name": "New Theorist",
        "perms": [
            {
                "desc": "•**Send images?** You can only send images in <#353766146115371009> and <#528253600682737685> once you reach <@&514113781903917056>. \n",
                "tags": [],
            }
        ],
    },
]
APPROVED_ROLES = [
    "Mods",
    "Server Glue",
    "Approved Theorizer",
    "Approved Artist",
    "Approved Writer",
    "Approved Musician",
    "Expert Meme-er",
]

BAD_WORD_PATTERN = re.compile(
    r"""
            \bn\W*[i*](i|\*|\W)*\W*[gb](g|b|\W)*(?!el|eria)(ers*|a|s|\W)*\b|
            \bs\W*p\W*[i*]+\W*c\W*(s|\W)*\b|
            \bf\W*[a*]+\W*[g]+\W*(got|\W)*(s|\W)*\b|
            \bc\W*[u*]+\W*m\W*s*\b|
            \bc\W*[u*]+\W*n\W*t\W*(s|\W)*\b|
            \bc\W*[o0*]\W*c\W*k\W*s*\b|
            \bp\W*u\W*[s$*]\W*[s$*]\W*(y|i|e|\W)+(s|\W)*\b|
            \bb\W*l\W*[o*]\W*w\W*j\W*o\W*b\W*(s|\W)*\b|
            \bc\W*h\W*[i*]\W*n\W*k\W*(s|\W)*\b|
            \bk\W*[i*]\W*k\W*e\W*(s|\W)*\b|
            \br*\W*[e*]*\W*t\W*[a*]\W*[r*]\W*d(ed|s|\W)*\b
            """,
    re.X | re.I,
)

VEGAS_LOG_CHANNEL_NAME = "vegas-logging"

EVERYONE_PATTERN = re.compile(r"@everyone")

BAN_APPEAL = "https://bit.ly/GTD-Ban-Appeal"

DATE_FORMAT = "%a, %b %-d, %Y @ %-I:%M %p"

WARN_COUNT_SQL = "select count(w.id) FROM discord_user_discorduser as u LEFT JOIN discord_user_discordwarn AS w ON u.id = w.discord_user_id WHERE u.discord_user_id=%s"

STRK_COUNT_SQL = "select count(s.id) FROM discord_user_discorduser as u LEFT JOIN discord_user_discordstrike AS s ON u.id = s.discord_user_id WHERE u.discord_user_id=%s"
POSTGRES_ERR = "{}: Please ensure Postgres and aiopg are installed; and local_settings.setup_postgres_pool and POSTGRES_AUTH are set"
REDIS_ERR = "{}: Please ensure Redis and aioredis are installed and local_settings.setup_redis is defined!"
NO_CHANNEL_MESSAGE = "Guild {} is not configured with `{}` for {}"

XP_IGNORED_CHANNELS = [
    "bot-spam",
    "the-lamp",
]
XP_IGNORED_CATEGORIES = [
    "Bot Commands",
    "Mod Logging",
    "Archived - Approved",
    "Archived",
    "Archived 2 - by grown up mods",
    "aPpRoVeD oThEr",
    "Approved Channels",
    "Serious Topics",
]
