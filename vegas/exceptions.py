from discord.ext.commands import CommandError


class PostgresNotInstalled(CommandError):
    pass


class RedisNotInstalled(CommandError):
    pass
