"""Basic Run file for VEGAS.

Allows someoene to manually start the bot as long as
requirements are setup."""
from settings.local_settings import TOKEN
from bot import client


def main():
    """Run the Bot!"""
    client.run(TOKEN)


main()
