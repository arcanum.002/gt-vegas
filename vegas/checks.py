"""This file provides multiple helper `checks` that make dealing with discord.py easier."""
import discord

from exceptions import PostgresNotInstalled
from exceptions import RedisNotInstalled

from settings.base import POSTGRES_ERR, REDIS_ERR


def has_user_ping():
    async def predicate(ctx):
        if ctx.message.mentions:
            return True
        await ctx.send("You have to provide a user ping!")
        return False

    return discord.ext.commands.check(predicate)


def doesnt_have_role(item):
    def predicate(ctx):
        if not isinstance(ctx.channel, discord.abc.GuildChannel):
            raise discord.ext.commands.NoPrivateMessage()

        if isinstance(item, int):
            role = discord.utils.get(ctx.author.roles, id=item)
        else:
            role = discord.utils.get(ctx.author.roles, name=item)
        if role is None:
            return True
        return False

    return discord.ext.commands.check(predicate)


def only_channel(item):
    def predicate(ctx):
        if not isinstance(ctx.channel, discord.abc.GuildChannel):
            raise discord.ext.commands.NoPrivateMessage()

        if isinstance(item, int):
            return item == ctx.message.channel.id
        else:
            return item == ctx.message.channel.name

    return discord.ext.commands.check(predicate)


def requires_postgres():
    def predicate(ctx):
        if not hasattr(ctx.bot, "pg_pool"):
            raise PostgresNotInstalled()
        return True

    return discord.ext.commands.check(predicate)


def requires_redis():
    def predicate(ctx):
        if not hasattr(ctx.bot, "redis"):
            raise RedisNotInstalled()
        return True

    return discord.ext.commands.check(predicate)


def setup_check_redis(bot, magic_file):
    if not hasattr(bot, "redis"):
        magic_file = magic_file.split("/")[-1]
        print(REDIS_ERR.format(magic_file))
        print("{} may not work properly...".format(magic_file))


def setup_check_postgres(bot, magic_file):
    if not hasattr(bot, "pg_pool"):
        magic_file = magic_file.split("/")[-1]
        print(POSTGRES_ERR.format(magic_file))
        print("{} may not work properly...".format(magic_file))
