"""Multiple helper functions for VEGAS"""
import datetime
import asyncio
import typing
from typing import Tuple


def asnyc_to_sync(coro: typing.Coroutine) -> typing.Any:
    """Allows async functions to run in non-async functions"""
    task = asyncio.create_task(coro)
    asyncio.get_running_loop().run_until_complete(task)
    return task.result()


def to_relative_delta(given_time: datetime.datetime):
    "Returns a string representing the time relative to now"""
    now = datetime.datetime.now()
    delta = now - given_time
    seconds = abs(int(delta.seconds))
    seconds += delta.days * 86400
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    # sets additional return parameters
    day = week = month = year = new = False
    if days == 0:
        new = True
    elif days >= 365:
        year = True
    elif days >= 31:
        month = True
    elif days >= 7:
        week = True
    elif days < 7:
        day = True
    # returns appropriate output string and bools for special Account age cases
    if days > 0:
        return "{} day(s) ago".format(days), new, day, week, month, year
    if hours > 0:
        return (
            "{} hour(s) and {} minute(s) ago".format(hours, minutes),
            new,
            day,
            week,
            month,
            year,
        )
    if minutes > 0:
        return "{} minute(s) ago".format(minutes), new, day, week, month, year
    if seconds > 0:
        return "{} second(s) ago".format(seconds), new, day, week, month, year
    return "bruh idek;\n now: {}\ngiven: {}\ndelta: {}".format(
        now, given_time, delta
    )


def process_timeout(timeout: str) -> Tuple[datetime.datetime, str]:
    """Processes a shorthand relative time format into a dateime
    object and returns the object and a string explaination of the
    format (e.g. week, month).

    Parameters
    ----------
    timeout: str
        The time format to use for the offest.

    Returns
    -------
    removal_time: datetime.datetime
        A datetime object representing when the role should be removed.
    time_format: str
        A string representation of the amount of time passing.
    """
    now = datetime.datetime.now()
    time_format = timeout[-1]
    time = int(timeout[:-1])

    if time_format == "m":
        time_offset = datetime.timedelta(minutes=time)
        time_format = "minutes"

    elif time_format == "h":
        time_offset = datetime.timedelta(hours=time)
        time_format = "hours"

    elif time_format == "d":
        time_offset = datetime.timedelta(days=time)
        time_format = "days"

    elif time_format == "w":
        time_offset = datetime.timedelta(weeks=time)
        time_format = "weeks"

    elif time_format == "M":
        mtime = time * 4
        time_offset = datetime.timedelta(weeks=mtime)
        time_format = "months"

    removal_time = now + time_offset

    return removal_time, time_format
