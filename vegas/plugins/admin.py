import typing
import datetime
import re

from typing import Union, List

from discord import Embed
from discord import Member
from discord import TextChannel
from discord.ext.commands import Context
from discord.ext import commands
from discord.utils import get
from discord.ext.commands import has_any_role
from discord.ext.commands import MemberConverter
from discord.ext.commands import RoleConverter
from discord.ext.commands import TextChannelConverter

from settings.base import APPROVED_ROLES as STRIKE_ROLES
from plugins.base import BasePlugin
from settings.base import COLORS
from checks import requires_postgres
from checks import setup_check_postgres


TIMEOUT = 40

COLOR = COLORS["ban"]

LOG_CHANNEL_NAME = "vegas-logging"

WARN_SQL = "SELECT * FROM discord_user_discordwarn WHERE discord_user_id=%s LIMIT %s;"
ROLE_SQL = "SELECT * FROM discord_user_discordrole WHERE discord_user_id=%s LIMIT %s;"
STRIKE_SQL = (
    "SELECT * FROM discord_user_discordstrike WHERE discord_user_id=%s LIMIT %s;"
)
KICK_SQL = "SELECT * FROM discord_user_discordkick WHERE discord_user_id=%s LIMIT %s;"
BAN_SQL = "SELECT * FROM discord_user_discordban WHERE discord_user_id=%s LIMIT %s;"
DFORMAT = "%m/%d/%Y %I:%M %p %Z"


class AdminPlugin(BasePlugin):
    def __init__(self, bot):
        self.bot = bot

    def event_logging_channel(self, channels: List[TextChannel]) -> TextChannel:
        return get(channels, name=LOG_CHANNEL_NAME)

    async def _build_single_embed(self, context: Context, user: Member, action: str, reason: str) -> Embed:
        if action == "warn":
            title = "{} has been warned!".format(user.name)
            field_name = "Warning Reason"
        if action == "strike":
            title = "{} has been struck!".format(user.name)
            field_name = "Strike Reason"
        if action == "kick":
            title = "{} has been kicked!".format(user.name)
            field_name = "Kick Reason"
        if action == "ban":
            title = "{} has been banned!".format(user.name)
            field_name = "Ban Reason"
        embed = Embed(title=title, color=COLOR)
        embed.add_field(name=field_name, value=reason, inline=False)
        embed.set_author(name=user.name, icon_url=user.avatar_url)
        # embed.set_footer(text="We're watching you :angrysquint:", icon_url=context.guild.icon)
        embed.set_footer(text="We're watching you")
        return embed

    async def _validate_member(self, context: Context, user) -> Member:
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)
        return user

    async def _validate_reason(self, context: Context, reason: Union[str, List[str]]) -> str:
        if reason:
            reason = " ".join(reason)
        else:
            reason = await self._collect_response(context, "reason", "string")
        return reason

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.id not in (
            691420795473494046,
            376171158497918976,
            692110917533696010,
        ):
            link_regex = re.compile(
                r"(?:https?://)?discord(?:app\.com/invite|\.gg)/?[a-zA-Z0-9]+/?",
                re.DOTALL,
            )
            message_links = re.findall(link_regex, message.content)
            # await message.channel.send(message_links)
            for element in message_links:
                if "discord.gg" in element or "discord.com/invite" in element:
                    link_invite = await self.bot.fetch_invite(element)
                    if link_invite.guild.id != 353694095220408322:
                        context = await self.bot.get_context(message)
                        embed = Embed(
                            title="Discord Invite Deleted",
                            color=COLOR,
                            description=message.author.mention
                            + "has posted a discord invite!",
                        )
                        embed.add_field(name="Invite Link", value=element, inline=True)
                        embed.set_author(
                            name=message.author.name, icon_url=message.author.avatar_url
                        )
                        embed.set_footer(text="We're watching you")

                        await message.delete()
                        await self.event_logging_channel(
                            context.message.guild.text_channels
                        ).send(embed=embed)
                        # await context.invoke(self.bot.get_command('warn'), self=self, context=context, username=message.author, echo_channel=message.channel.mention, reason="No discord invites allowed.")
                        # not super confident in the above line, im guessing there will be errors when warn is invoked
                        # because v!warn gets its member based on context.message.mentions[0] and there are no mentions in this message,
                        # but the rest should be intact

    @commands.group(
        description="Check a users Warns/Kicks/Roles/Bans",
        case_insensitive=True,
        aliases=[
            "u",
        ],
    )
    @has_any_role("Can Warn")
    @requires_postgres()
    async def user(self, context):
        if context.invoked_subcommand is None:
            guild = context.guild
            member = context.message.mentions[0]
            fmt = "{date} - **{reason}**\n"

            link_id = await self._check_user_exists(member.id, guild.id)

            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(WARN_SQL, (link_id, 5))
                    warns = await cursor.fetchall()

                    await cursor.execute(STRIKE_SQL, (link_id, 5))
                    strikes = await cursor.fetchall()

                    await cursor.execute(ROLE_SQL, (link_id, 5))
                    roles = await cursor.fetchall()

                    await cursor.execute(KICK_SQL, (link_id, 3))
                    kicks = await cursor.fetchall()

                    await cursor.execute(BAN_SQL, (link_id, 2))
                    bans = await cursor.fetchall()

            embed = Embed(title="{} Infractions".format(member.name), color=COLOR)
            embed.set_thumbnail(url=member.avatar_url_as(static_format="png"))

            embed.add_field(name="Total Warns:", value=str(len(warns)), inline=True)
            embed.add_field(name="Total Strikes:", value=str(len(strikes)), inline=True)
            embed.add_field(name="Total Roles:", value=str(len(roles)), inline=True)
            embed.add_field(name="Total Kicks:", value=str(len(kicks)), inline=True)
            embed.add_field(name="Total Bans:", value=str(len(bans)), inline=True)

            fmt_str = "[None]"
            for warn in warns:
                if len(fmt_str) <= 1000:
                    date = warn[1].strftime(DFORMAT)
                    if fmt_str == "[None]":
                        fmt_str = fmt.format(date=date, reason=warn[2])
                    else:
                        fmt_str += fmt.format(date=date, reason=warn[2])
            embed.add_field(name="Warns", value=fmt_str, inline=False)

            fmt_str = "[None]"
            for strike in strikes:
                if len(fmt_str) <= 1000:
                    strike_channel = guild.get_role(int(strike[3]))
                    date = strike[1].strftime(DFORMAT)
                    strike_reason = strike_channel.name + " - " + strike[2]
                    if fmt_str == "[None]":
                        fmt_str = fmt.format(date=date, reason=strike_reason)
                    else:
                        fmt_str += fmt.format(date=date, reason=strike_reason)
            embed.add_field(name="Strikes", value=fmt_str, inline=False)

            fmt_str = "[None]"
            for role in roles:
                if "requested" not in role[2].lower():
                    if len(fmt_str) <= 1000:
                        date = role[1].strftime(DFORMAT)
                        grole = guild.get_role(int(role[4]))
                        date = role[3].strftime(DFORMAT)

                        if fmt_str == "[None]":
                            if role[6]:
                                fmt_str = "**++{}** {}\n".format(grole.name, date)
                            else:
                                fmt_str = "**--{}** on {}\n".format(grole.name, date)
                        else:
                            if role[6]:
                                fmt_str += "**++{}** {}\n".format(grole.name, date)
                            else:
                                fmt_str += "**--{}** on {}\n".format(grole.name, date)

            embed.add_field(name="Roles", value=fmt_str, inline=False)

            fmt_str = "[None]"
            for kick in kicks:
                if len(fmt_str) <= 1000:
                    date = kick[1].strftime(DFORMAT)
                    if fmt_str == "[None]":
                        fmt_str = fmt.format(date=date, reason=kick[2])
                    else:
                        fmt_str += fmt.format(date=date, reason=kick[2])

            embed.add_field(name="Kicks", value=fmt_str, inline=False)

            fmt_str = "[None]"
            for ban in bans:
                if len(fmt_str) <= 1000:
                    date = ban[1].strftime("%m-%d-%Y %I:%M %p %Z")
                    if fmt_str == "[None]":
                        fmt_str = fmt.format(date=date, reason=ban[2])
                    else:
                        fmt_str += fmt.format(date=date, reason=ban[2])

            embed.add_field(name="Bans:", value=fmt_str, inline=False)

            await context.send(embed=embed)

    @user.command(
        description="Check a users Roles",
        case_insensitive=True,
        aliases=[
            "ur",
        ],
    )
    @has_any_role("Can Warn")
    async def user_roles(self, context, username):
        guild = context.guild
        member = context.message.mentions[0]

        link_id = await self._check_user_exists(member.id, guild.id)

        embed = Embed(title="{} Warns".format(member.name), color=COLOR)
        embed.set_thumbnail(url=member.avatar_url)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(ROLE_SQL, (link_id, 25))
                roles = await cursor.fetchall()

        for role in roles:
            if "requested" not in role[2].lower():
                grole = guild.get_role(int(role[4]))
                date_recieved = role[1].strftime(DFORMAT)
                date_removed = role[3].strftime(DFORMAT)
                reason_tpl = "*Recieved:* {}\n*Removed:* {}\n**Reason:**\n{}"
                reason = reason_tpl.format(date_recieved, date_removed, role[2])

            embed.add_field(name=grole.name, value=reason, inline=False)

        await context.send(embed=embed)

    @user.command(
        description="Check a users Warns",
        case_insensitive=True,
        aliases=[
            "uw",
        ],
    )
    @has_any_role("Can Warn")
    async def user_warns(self, context, username):
        guild = context.guild
        member = context.message.mentions[0]

        link_id = await self._check_user_exists(member.id, guild.id)

        embed = Embed(title="{} Warns".format(member.name), color=COLOR)
        embed.set_thumbnail(url=member.avatar_url)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(WARN_SQL, (link_id, 25))
                warns = await cursor.fetchall()

        for warn in warns:
            date = warn[1].strftime(DFORMAT)
            embed.add_field(name=date, value=warn[2], inline=False)

        await context.send(embed=embed)

    @commands.command(
        description="Check a users strikes",
        case_insensitive=True,
        aliases=[
            "ls",
        ],
    )
    @has_any_role(*STRIKE_ROLES)
    @requires_postgres()
    async def user_strikes(self, context, username):
        guild = context.guild
        member = context.message.mentions[0]
        fmt = "{role} - {date}"
        link_id = await self._check_user_exists(member.id, guild.id)

        embed = Embed(title="{} Strikes".format(member.name), color=COLOR)
        embed.set_thumbnail(url=member.avatar_url)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(STRIKE_SQL, (link_id, 25))
                strikes = await cursor.fetchall()

        for strike in strikes:
            strike_role = guild.get_role(int(strike[3]))
            date = strike[1].strftime(DFORMAT)
            field_name = fmt.format(role=strike_role.name, date=date)
            embed.add_field(name=field_name, value=strike[2], inline=False)

        await context.send(embed=embed)

    @commands.command(
        description="@Can Warn command to interacatively warn a user.",
        case_insensitive=True,
        aliases=[
            "iw",
        ],
    )
    @has_any_role("Can Warn")
    @requires_postgres()
    async def interactive_warn(
        self, context, user=None, echo_channel=None, *reason: typing.Optional
    ):
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            user = await self._collect_response(context, "", commands.MemberConverter)
            if not user:
                return
        try:
            if echo_channel != "-":
                echo_channel = await TextChannelConverter().convert(
                    ctx=context, argument=echo_channel
                )
        except (commands.ChannelNotFound, TypeError):
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )
            if not echo_channel:
                return
        reason = await self._validate_reason(context, reason)
        if not reason:
            return
        confirm_message = await context.send(
            f"{user.mention} will be warned in {echo_channel.mention} for `{reason}`"
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed or not user or not not echo_channel or not reason:
            return

        warn_cmd = self.bot.get_command("warn")
        
        if echo_channel != "-":
            await context.invoke(warn_cmd, user.mention, echo_channel.mention, reason)
        else:
            await context.invoke(warn_cmd, user.mention, "-", reason)

    @commands.command(
        description="@Can Warn command to warn a user",
        case_insensitive=True,
        aliases=[
            "w",
        ],
    )
    @has_any_role("Can Warn")
    @requires_postgres()
    async def warn(self, context, username, echo_channel, *reason):
        sql = "INSERT INTO discord_user_discordwarn(date_recieved, reason, discord_user_id) VALUES(%s, %s, %s);"
        guild = context.guild
        member = await MemberConverter().convert(ctx=context, argument=username)
        now = datetime.datetime.now()
        reason = " ".join(reason)
        link_id = await self._check_user_exists(member.id, guild.id)

        await context.message.delete()

        if not reason:
            await context.send("Please provide a reason!")
            return

        if context.message.author.top_role.position < member.top_role.position:
            await context.send("You cannot warn someone with a higher role!")
            return

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(sql, (now, reason, link_id))
        embed = await self._build_single_embed(context, member, "warn", reason)
        await context.send(embed=embed)
        if echo_channel:
            echo_channel = get(guild.text_channels, mention=echo_channel)
            await echo_channel.send(content=member.mention, embed=embed)

    @commands.command(
        description="Any Approved can run this command to interatively strike a user",
        case_insensitive=True,
        aliases=[
            "is",
        ],
    )
    @has_any_role(*STRIKE_ROLES)
    @requires_postgres()
    async def interactive_strike(
        self,
        context,
        user=None,
        strike_type=None,
        echo_channel=None,
        *reason: typing.Optional,
    ):
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)
        try:
            strike_type = await RoleConverter().convert(ctx=context, argument=user)
        except (commands.RoleNotFound, TypeError):
            await context.send(f"I couldn't find role {strike_type} not found!")
            strike_type = await self._collect_response(
                context, "", commands.RoleConverter
            )
        try:
            if echo_channel != "-":
                echo_channel = await TextChannelConverter().convert(
                    ctx=context, argument=echo_channel
                )
        except (commands.ChannelNotFound, TypeError):
            await context.send(f"I couldn't find channel {echo_channel} not found!")
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )
        reason = await self._validate_reason(context, reason)
        confirm_message = await context.send(
            f"{user.mention} will be struck in {echo_channel.mention} for `{reason}`"
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        strike_cmd = self.bot.get_command("strike")
        if echo_channel != "-":
            await context.invoke(
                strike_cmd, user.mention, strike_type.name, echo_channel.mention, reason
            )
        else:
            await context.invoke(
                strike_cmd, user.mention, strike_type.name, "-", reason
            )

    @commands.command(
        description="Any Approved can run this command to strike a user",
        case_insensitive=True,
        aliases=[
            "s",
        ],
    )
    @has_any_role(*STRIKE_ROLES)
    @requires_postgres()
    async def strike(self, context, username, strike_type, echo_channel, *reason):
        sql = "INSERT INTO discord_user_discordstrike(date_recieved, reason, strike_type, discord_user_id) VALUES(%s, %s, %s, %s);"
        guild = context.guild
        roles = guild.roles
        member = await MemberConverter().convert(ctx=context, argument=username)
        now = datetime.datetime.now()
        link_id = await self._check_user_exists(member.id, guild.id)

        await context.message.delete()

        reason = " ".join(reason)
        strike_type = get(roles, name=strike_type)

        if context.message.author.top_role.position < member.top_role.position:
            await context.send("You cannot strike someone with a higher role!")
            return

        if not reason:
            await context.send("Please provide a reason!")
            return

        if not strike_type:
            await context.send("I need a valid role to strike for!")
            return

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(sql, (now, reason, strike_type.id, link_id))
        embed = await self._build_single_embed(context, member, "strike", reason)
        await context.send(embed=embed)
        if echo_channel:
            echo_channel = get(guild.text_channels, mention=echo_channel)
            await echo_channel.send(content=member.mention, embed=embed)

    @commands.command(
        description="@Can Kick command to Kick a user interactively",
        case_insensitive=True,
        aliases=[
            "ik",
        ],
    )
    @has_any_role("Can Kick")
    @requires_postgres()
    async def interactive_kick(
        self, context, user=None, echo_channel=None, *reason: typing.Optional
    ):
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)
        try:
            if echo_channel != "-":
                echo_channel = await TextChannelConverter().convert(
                    ctx=context, argument=echo_channel
                )
        except (commands.ChannelNotFound, TypeError):
            await context.send(f"I couldn't find channel {echo_channel} not found!")
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )
        reason = await self._validate_reason(context, reason)
        confirm_message = await context.send(
            f"{user.mention} will be kicked in {echo_channel.mention} for `{reason}`"
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        kick_cmd = self.bot.get_command("kick")
        if echo_channel != "-":
            await context.invoke(kick_cmd, user.mention, echo_channel.mention, reason)
        else:
            await context.invoke(kick_cmd, user.mention, "-", reason)

    @commands.command(
        description="@Can Kick command to Kick a user",
        case_insensitive=True,
        aliases=[
            "k",
        ],
    )
    @has_any_role("Can Kick")
    @requires_postgres()
    async def kick(self, context, username, echo_channel, *reason):
        sql = "INSERT INTO discord_user_discordkick(date_recieved, reason, discord_user_id) VALUES(%s, %s, %s);"
        """
            TODO: Add cache for `rejoin` later
        """
        guild = context.guild
        member = await MemberConverter().convert(ctx=context, argument=username)
        now = datetime.datetime.now()
        reason = " ".join(reason)
        link_id = await self._check_user_exists(member.id, guild.id)

        await context.message.delete()

        if context.message.author.top_role.position < member.top_role.position:
            await context.send("You cannot kick someone with a higher role!")
            return

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await member.kick(reason=reason)
                await cursor.execute(sql, (now, reason, link_id))
        embed = await self._build_single_embed(context, member, "kick", reason)
        await context.send(embed=embed)
        if echo_channel:
            echo_channel = get(guild.text_channels, mention=echo_channel)
            await echo_channel.send(content=member.mention, embed=embed)

    @commands.command(
        description="@Can Ban command to Ban a user interactively",
        case_insensitive=True,
        aliases=[
            "ib",
        ],
    )
    @has_any_role("Can Ban")
    @requires_postgres()
    async def interactive_ban(
        self, context, user=None, echo_channel=None, *reason: typing.Optional
    ):
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)
        try:
            if echo_channel != "-":
                echo_channel = await TextChannelConverter().convert(
                    ctx=context, argument=echo_channel
                )
        except (commands.ChannelNotFound, TypeError):
            await context.send(f"I couldn't find channel {echo_channel} not found!")
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )
        reason = await self._validate_reason(context, reason)
        confirm_message = await context.send(
            f"{user.mention} will be kicked in {echo_channel.mention} for `{reason}`"
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        ban_cmd = self.bot.get_command("kick")
        if echo_channel != "-":
            await context.invoke(ban_cmd, user.mention, echo_channel.mention, reason)
        else:
            await context.invoke(ban_cmd, user.mention, "-", reason)

    @commands.command(
        description="@Can Ban command to ban a user",
        case_insensitive=True,
        aliases=[
            "b",
        ],
    )
    @has_any_role("Mods", "Can Ban")
    @requires_postgres()
    async def ban(self, context, username, echo_channel, *reason):
        BAN_APPEAL = "https://bit.ly/Ban-Appeal-GTD"
        sql = "INSERT INTO discord_user_discordban(date_recieved, reason, discord_user_id) VALUES(%s, %s, %s);"
        """
            TODO: Add cache for `rejoin` later
        """
        reason_format = "{reason} - Ban Appeal {form}"
        guild = context.guild
        member = context.message.mentions[0]
        now = datetime.datetime.now()
        reason = " ".join(reason)
        reason = reason_format.format(reason=reason, form=BAN_APPEAL)
        link_id = await self._check_user_exists(member.id, guild.id)
        degendex_chan = get(guild.text_channels, name="degendex")

        await context.message.delete()

        if context.message.author.top_role.position < member.top_role.position:
            await context.send("You cannot ban someone with a higher role!")
            return

        await member.send(reason)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await member.ban(reason=reason, delete_message_days=0)
                await cursor.execute(sql, (now, reason, link_id))

        await context.send("User has been banned: " + reason)
        embed = await self._build_single_embed(context, member, "ban", reason)
        await context.send(embed=embed)
        await degendex_chan.send(embed=embed)
        if echo_channel:
            echo_channel = get(guild.text_channels, mention=echo_channel)
            await echo_channel.send(content=member.mention, embed=embed)

    @commands.command(
        description="Removes a legacy role form users", case_insensitive=True
    )
    @has_any_role("Mods")
    @requires_postgres()
    async def remove_hrtr(self, context):
        hrtr = await commands.RoleConverter().convert(
            ctx=context, argument="Haven't Read the Rules"
        )
        if hrtr is None:
            await context.send("I can't find that role!")
            return
        elif len(hrtr.members) == 0:
            await context.send("No members with that role!")
            return

        for member in hrtr.members:
            await member.send("Test message")
            await member.remove_roles(hrtr, reason="Removed for legacy purge")
            await context.send(
                "{} was removed from {}".format(member.mention, hrtr.name)
            )
        await context.send("Done!")


def setup(bot):
    setup_check_postgres(bot, __file__)
    bot.add_cog(AdminPlugin(bot))
