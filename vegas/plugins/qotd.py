import functools
import datetime
import random

import discord
from discord.ext import commands

from discord.ext.commands import has_any_role

from .base import BasePlugin

from settings.base import APPROVED_ROLES

from checks import requires_postgres
from checks import setup_check_postgres

ROLES = ["Can QotD", "Can Mod"]
COLOURS = [
    0xDE392E,
    0xDF9326,
    0xE6E13D,
    0x37CB39,
    0x3D9DCE,
    0x5640D2,
]
EMBED_DESC = "Every day we post a question that is thought provoking, educational, or just fun! Answer in this channel!"
EMBED_ICON = "https://cdn.discordapp.com/attachments/503665005015597066/596008440103567481/image0.png"
EMBED_PING = "<@&505157047634100274>"


class QOTDPlugin(BasePlugin):
    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if not hasattr(self.bot, "pg_pool"):
            return
        channel = self.bot.get_channel(payload.channel_id)
        if isinstance(channel, discord.DMChannel):
            return
        sql_insert = "INSERT INTO qotd_qotdanswer (content, avatar_url, username, user_id, qotd_id, message_id, channel_id) VALUES (%s, %s, %s, %s, %s, %s, %s);"
        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)

        if (
            channel.name == "question-of-the-day" or channel.name == "vegas-logging"
        ) and str(payload.emoji) == "❔":
            if any(srole.name in APPROVED_ROLES for srole in message.author.roles):
                qotd_id_sql = "SELECT id FROM qotd WHERE date_posted::date = '{}' ORDER BY date_added asc LIMIT 1;".format(
                    datetime.date.today()
                )

                async with self.bot.pg_pool.acquire() as conn:
                    async with conn.cursor() as cur:
                        await cur.execute(qotd_id_sql)
                        qotd_id = await cur.fetchone()
                        message = await channel.fetch_message(payload.message_id)
                        mention = (
                            message.author.name + "#" + message.author.discriminator
                        )
                        avatar = str(message.author.avatar_url)
                        user_id = str(message.author.id)
                        message_id = str(message.id)
                        channel_id = str(channel.id)
                        await cur.execute(
                            sql_insert,
                            (
                                message.content,
                                avatar,
                                mention,
                                user_id,
                                qotd_id,
                                message_id,
                                channel_id,
                            ),
                        )

    @commands.group(
        description="All about Question of the Day!",
        case_insensitive=True,
        aliases=[
            "question_of_the_day",
        ],
    )
    @requires_postgres()
    async def qotd(self, context):
        getter = functools.partial(discord.utils.get, context.author.roles)
        if any(
            getter(id=item) is not None
            if isinstance(item, int)
            else getter(name=item) is not None
            for item in ROLES
        ):
            if context.invoked_subcommand is None:
                await context.send("What do you want to do with Question of the Day?")
        else:
            await context.send(
                "Sorry you don't have the permissions to use this command"
            )

    @qotd.command(
        name="add_question",
        description="Add a question to the queue for QotD",
        brief="Add a question to the queue for QotD",
        aliases=["a", "+"],
    )
    @has_any_role("Admin", "Can QotD")
    async def add_question(self, context, question, author=None, date_post=None):
        sql_format = "INSERT INTO qotd ({fields_format}) VALUES ({values_format})"
        today = datetime.date.today().isoformat()

        fields = "question, date_added"
        values = "%s, %s"
        value_insert = [question, today]

        if not question:
            await context.send("Please provide a question!")

        if author:
            values += ", %s"
            fields += ", author"
            value_insert.append(author)
        if date_post:
            values += ", %s"
            fields += ", date_posted"
            value_insert.append(date_post)

        sql = sql_format.format(fields_format=fields, values_format=values)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql, value_insert)
                await context.send("Thanks for the question!")

    @qotd.command(
        name="post_question",
        description="Post the the Question of the Day",
        brief="Post the question for QotD",
        aliases=[
            "p",
        ],
    )
    @has_any_role("Admin", "Can QotD")
    async def post_question(self, context, selector=None, ping_role=True, test=False):
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:

                if selector == "today":
                    sql = "SELECT * FROM qotd WHERE date_posted::date = '{}' ORDER BY date_added asc LIMIT 1;".format(
                        datetime.date.today()
                    )
                    await cur.execute(sql)
                elif int(selector):
                    sql = "SELECT * FROM qotd WHERE id = %s LIMIT 1;"
                    await cur.execute(sql, selector)
                ret = await cur.fetchone()

                if ret is None:
                    sql = "SELECT * FROM qotd WHERE qotd.archived = false ORDER BY date_added asc LIMIT 1;"
                    await cur.execute(sql)
                    ret = await cur.fetchone()

                embed = discord.Embed(
                    title="Question of the Day!",
                    description=EMBED_DESC,
                    colour=random.choice(COLOURS),
                )
                embed.add_field(name="Question", value=ret[1], inline=False)
                embed.add_field(name="Author", value=ret[2], inline=True)
                embed.add_field(name="Added", value=ret[3], inline=True)
                if test is True:
                    embed.add_field(name="Test", value="True", inline=True)
                embed.set_thumbnail(url=EMBED_ICON)
                if ping_role is True:
                    message = await context.send(EMBED_PING, embed=embed)
                else:
                    message = await context.send(embed=embed)
                await message.pin()
                if test is False:
                    await cur.execute(
                        "UPDATE qotd SET archived=TRUE WHERE id={}".format(ret[0])
                    )

    @qotd.command(
        name="list_questions",
        description="List the the Question of the Day questions",
        brief="List the questions for QotD",
        aliases=[
            "l",
        ],
    )
    @has_any_role("Admin", "Can QotD")
    async def list_questions(self, context, limit=50):
        sql = "SELECT * FROM qotd WHERE qotd.archived = false ORDER BY date_added asc LIMIT {};".format(
            limit
        )
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql)
                questions = await cur.fetchall()
                embed = discord.Embed(
                    title="Question of the Day! List",
                    description=EMBED_DESC,
                    colour=random.choice(COLOURS),
                )
                embed.set_thumbnail(url=EMBED_ICON)
                for question in questions:
                    name_format = "ID: {id} - Date Added: {date_added} - Date to Post: {date_post}".format(
                        id=question[0], date_added=question[3], date_post=question[4]
                    )
                    value_format = (
                        "Author: {author}\nQuestion: {question}\n-----".format(
                            question=question[1], author=question[2]
                        )
                    )
                    embed.add_field(name=name_format, value=value_format, inline=False)
                await context.send(embed=embed)

    @qotd.command(
        name="search_questions",
        description="Search the the Question of the Day question queue",
        brief="Search the questions for QotD",
        aliases=[
            "s",
        ],
    )
    @has_any_role("Admin", "Can QotD")
    async def search_questions(self, context, search_term, limit=50):
        sql = "SELECT * FROM qotd WHERE qotd.archived = false AND question ILIKE '%{}%' ORDER BY date_added asc LIMIT {};".format(
            search_term, limit
        )
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql)
                questions = await cur.fetchall()
                embed = discord.Embed(
                    title="Question of the Day! Results",
                    description=EMBED_DESC,
                    colour=random.choice(COLOURS),
                )
                embed.set_thumbnail(url=EMBED_ICON)
                for question in questions:
                    name_format = "ID: {id} - Date Added: {date_added} - Date to Post: {date_post}".format(
                        id=question[0], date_added=question[3], date_post=question[4]
                    )
                    value_format = (
                        "Author: {author}\nQuestion: {question}\n-----".format(
                            question=question[1], author=question[2]
                        )
                    )
                    embed.add_field(name=name_format, value=value_format, inline=False)
                await context.send(embed=embed)

    @qotd.command(
        name="delete_question",
        description="Delete a question form the QotD queue",
        brief="Delete a question form the QotD queue",
        aliases=[
            "d",
        ],
    )
    @has_any_role("Admin", "Can QotD")
    async def delete_question(self, context, qid=None):
        if qid is None:
            await context.send("Please provide Question ID!")
        sql = "DELETE FROM qotd WHERE id={}".format(qid)
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql)
        await context.send("Question {} deleted!".format(qid))


def setup(bot):
    setup_check_postgres(bot, __file__)
    bot.add_cog(QOTDPlugin(bot))
