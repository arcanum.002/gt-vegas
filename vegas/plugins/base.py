"""This class can be used to provide the base "plugin" functionality for VEGAS."""
import asyncio
from typing import Union
from functools import partial

import discord
from discord import User, Member
from discord.ext import commands
from discord.ext.commands import MemberConverter
from discord.ext.commands import RoleConverter
from discord.ext.commands import TextChannelConverter
from discord.ext.commands import EmojiConverter


TIMEOUT = 30.0


class BasePlugin(commands.Cog):
    """BasePlugin provides a set of useful methods for other plugins"""
    def __init__(self, bot):
        self.bot = bot

    async def _get_guild(self, context):
        """Private method that returns a guild

        This method will first check the provided `context.guild` for a `discord.Guild` object. If
        one cannot be found (such as the user is in a DMChannel), the user will be prompted to
        provide a `discord.Guild.id`. The object is then lookedup and returned.

        Parameters
        ----------
        context: discord.Context
            A discordpy Context object.

        Returns
        -------
            guild: discord.Guild if successful or None
                If the user provides a `discord.Guild.id` to a guild the bot is in, the Guild is returned,
                otherwise None is returned.

        """
        if context.guild:
            return context.guild

        await context.send("Please send a Server ID:")

        check_is_dm_and_author_message = partial(
            self._check_is_dm_and_author_message_partial, context.message.author
        )

        while True:
            try:
                message = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                if message.content.lower() == "cancel":
                    await context.send("Thanks for using {}".format(self.bot.user.name))
                    break
                guild = self.bot.get_guild(int(message.content))
                if not guild:
                    await context.send("I am not in a guild with that ID.")
                return guild
            except asyncio.TimeoutError:
                await context.send("If you'd like to cancel, reply with `Cancel`")
            except ValueError:
                await context.send(
                    "I need a Server ID (a number) to lookup, or reply with `Cancel` to quit."
                )

    def _check_is_dm_and_author_reaction_partial(
        self,
        caller: Union[User, Member],
        reaction: discord.Reaction,
        user: Union[User, Member],
    ) -> bool:
        """Ensures the user is in a DMChannel"""
        return caller == user and isinstance(
            reaction.message.channel, discord.DMChannel
        )

    def _check_is_dm_and_author_message_partial(
        self, member: Union[Member, User], message: discord.Message
    ) -> bool:
        """Ensures the Caller is the author and is in a DMChannel"""
        return member == message.author and isinstance(
            message.channel, discord.DMChannel
        )

    def _check_is_author_reaction_partial(self, context, _, user) -> bool:
        return context.message.author == user

    def _check_is_author_reaction_on_message_partial(
        self, context, message, reaction, user
    ) -> bool:
        return context.message.author == user and message == reaction.message

    async def _check_user_exists(self, user_id, guild_id):
        """Private method to ensure a users discord_user_discorduser record exists.

        This method accepts a users id and a guild id. The SQL that gets executed
        will first check to see if a record for this user/guild exists. If the
        record exsists, that id is returned. If no record exists, a new record is
        created with the user/guild id combo and that new id is returned.

        Parameters
        ----------
        user_id: int
            The discord.Member.id of a guild member.
        guild_id: int
            A discord.Guild.id.

        Returns
        -------
        link_id: int
            The id of the users discord_user_discorduser record
        """
        sql_insert = """with s as (
                            select id
                            from discord_user_discorduser
                            where discord_guild_id = '%s' and discord_user_id = '%s'
                        ), i as (
                            insert into discord_user_discorduser ("discord_guild_id", "discord_user_id")
                            select '%s', '%s'
                            where not exists (select 1 from s)
                            returning id
                        )
                        select id
                            from i
                        union all
                        select id
                            from s"""
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(sql_insert, (guild_id, user_id, guild_id, user_id))
                link_id = await cursor.fetchone()
                return link_id[0]

    def _check_is_author_message_partial(self, member, message):
        """Ensures the Caller is the Mod Contact author and is in a DMChannel"""
        return member == message.author

    async def _confirm_with_reaction(self, context, message):
        check_is_author_reaction = partial(
            self._check_is_author_reaction_on_message_partial, context, message
        )
        while True:
            try:
                reaction, _ = await self.bot.wait_for(
                    "reaction_add", timeout=TIMEOUT, check=check_is_author_reaction
                )
                if reaction.emoji == "👍":
                    confirm_emoji = await EmojiConverter().convert(
                        ctx=context, argument="vegasfingerguns"
                    )
                    await message.edit(content=confirm_emoji)
                    return False
                elif reaction.emoji == "❌":
                    no_emoji = await EmojiConverter().convert(
                        ctx=context, argument="vegaspensive"
                    )
                    await message.edit(content=no_emoji)
                    return True
            except asyncio.TimeoutError:
                await context.send("Your session timed out.")
                return

    async def _collect_response(self, context, validator_type: str, convert_type):
        check_is_author_message = partial(
            self._check_is_author_message_partial, context.message.author
        )
        if convert_type is MemberConverter:
            validator_type = "Member"
        elif convert_type is RoleConverter:
            validator_type = "Role"
        elif convert_type is TextChannelConverter:
            validator_type = "Channel"
        try:
            while True:
                await context.send(f"Please provide a {validator_type}")
                response = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_author_message
                )
                if response.content == "❌":
                    await context.send("Cancelling!")
                    return
                try:
                    if convert_type is TextChannelConverter and response.content == "-":

                        class DummyChan:
                            """Dummy Channel to provide a method call"""
                            def __init__(self):
                                self.mention = "-"

                        return DummyChan()
                    if convert_type != "string":
                        lookup = await convert_type().convert(
                            ctx=context, argument=response.content
                        )
                    else:
                        lookup = response.content
                    return lookup
                except commands.BadArgument as bexception:
                    await context.send(bexception.args[0])
                    continue
        except asyncio.TimeoutError:
            await context.send("Your session timed out.")
            return
