"""This COG supplies the base and foundation for levled roles.

LVL_MESSAGES: List[str]
    List of rank up messages containing both {user} and {rank} keys.
DIV_LVL_MESSAGES: List[str]
    List of messages of Divine Rank up only. Must contain {user} and {rank}.
INSERT_LR_SQL: str
    SQL query to create a new user score record.
SELECT_LR_SQL: str
   Requires guild_id. SQL query returning all users from a guild
SELECT_LR_STARTUP_SQL: str
    SQL that returns all users in DB sorted by score.
REDIS_LR_KEY: str
    A format key to access a redis hash. Requires Guild ID.
"""
import random

import discord
from discord.ext import commands
from discord.ext import tasks
from discord.utils import get

from .base import BasePlugin

from checks import requires_redis
from checks import requires_postgres
from checks import setup_check_postgres
from checks import setup_check_redis

LVL_MESSAGES = [
    "{user} just advanced to {rank}! Congrats! 🎉",
    # "{user}, congrats nerd for talking too much. You're a {rank} now",
    "When I was your age {user}, I was already Divine, but take {rank} anyways for good measure",
    # "{user}. You've been here how long and just now hit {rank}? Weak.",
    "Hey, {user} just ranked up to {rank}! Give them a clap and a half! :aClap: :andAHalf:",
    "{User} just advanced to {rank)! Congrats! :tada:",
    "{User}, congrats! You're a {rank) now",
    # "{User}, Congrats for achieving {Divine Theorist|! Prestige and do it again <:zoomeyes:695120566696411236>",
    "Hey, {User} just ranked up to {rank}! Give them a clap and a half! <:aClap:695120552708276254> <:andAHalf:695120551202521139>",
    "Hey! {User} just ranked up to {rank}! <a:a_ChrisDance:708048911046017099>",
    "Hey! {User} just ranked up to {rank}! Give them a clap and a half!<a:a_clapandahalf:695120567996514325>",
    "<:birbe:695120552611807313> {User} just ranked up to {rank}! Congrats!",
    "{User} is now {rank}!",
    "Hey now, {User}'s a {Rank}. Get your game on, go play :musical_note:",
    # "Hey {user}! You just ranked up to {rank}! You can also now do  (all ranks with perks)",
]

DIV_LVL_MESSAGES = [
    "{user}, Congrats for achieving . Prestige and do it again, coward. (Divine Only)",
]

INSERT_LR_SQL = "INSERT INTO discord_xp_leveledrole (guild, name, xp_required) VALUES ( %s, %s, %s);"
SELECT_LR_SQL = (
    "SELECT * FROM discord_xp_leveledrole WHERE guild = %s ORDER BY xp_required DESC;"
)
SELECT_LR_STARTUP_SQL = (
    "SELECT * FROM discord_xp_leveledrole ORDER BY guild ASC, xp_required DESC;"
)

REDIS_LR_KEY = "{}:leveled_roles"


class LeveledRolesPlugin(BasePlugin):
    def __init__(self, bot):
        """Startup the bot and launch intitial startup_load

        Uses
        ----
        self.startup_load
        """
        super().__init__(bot)
        self.bot = bot
        self.startup_load.start()

    @tasks.loop(minutes=1.0, count=1)
    async def startup_load(self):
        """Runs one time when the cog loads. Loads leveled roles into Redis

        Uses
        ----
        - self.bot.redis
        - SELECT_LR_STARTUP_SQL
        - REDIS_LR_KEY
        """
        if not hasattr(self.bot, "pg_pool"):
            return
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(SELECT_LR_STARTUP_SQL)
                leveled_roles = await cur.fetchall()
        for lrole in leveled_roles:
            guild_id = lrole[1]
            lname = lrole[2]
            lscore = lrole[3]
            key = REDIS_LR_KEY.format(guild_id)
            await self.bot.redis.hsetnx(key, lscore, lname)

    @staticmethod
    async def level_user(
        bot, channel: discord.TextChannel, member: discord.Member, score: int
    ) -> None:
        """Static method to check if the provided member with provided score should level up

        Parameters
        ----------
        channel: discord.TextChannel
            The channel that messages should be sent to.
        member: discord.Member
            The member to check leveles/score against.
        score: int
            The users score to check against

        Uses
        ----
        - self.bot.redis
        - REDIS_LR_KEY
        """
        key = REDIS_LR_KEY.format(channel.guild.id)
        guild_leveled_roles = await bot.redis.hgetall(key, encoding="utf-8")
        leveled_up = False
        for role_score, role_name in guild_leveled_roles.items():
            if score > int(role_score):
                role = get(channel.guild.roles, name=role_name)

                if not leveled_up and role not in member.roles:
                    level_up_message_fmt = random.choice(LVL_MESSAGES)
                    await member.add_roles(role, reason="Leveled up!")
                    message = await channel.send(
                        level_up_message_fmt.format(user=member.mention, rank=role.name)
                    )
                    await message.edit(
                        content=level_up_message_fmt.format(
                            user=member.mention, rank=role.mention
                        )
                    )
                elif leveled_up and role in member.roles:
                    await member.remove_roles(role, reason="Leveled up!")

                leveled_up = True

    @commands.group(
        aliases=[
            "lr",
        ],
    )
    @commands.guild_only()
    @requires_redis()
    @requires_postgres()
    async def leveled_roles(self, context):
        """Placehodler command to allow grouping"""
        if context.invoked_subcommand is None:
            await context.send("Please use a subcommand!")

    @leveled_roles.command()
    @commands.guild_only()
    @commands.has_any_role("Mods")
    async def list(self, context):
        """Displays a list of leveled roles and the amount of XP required.

        Uses
        ----
        - self.pg_pool
        - SELECT_LR_SQL
        """
        embed = discord.Embed(
            title="Leveled Roles for {}".format(context.guild.name),
            color=context.author.top_role.color,
        )
        embed.set_thumbnail(url=context.guild.icon_url)
        if context.invoked_subcommand is None:
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(SELECT_LR_SQL, (context.guild.id,))
                    leveled_roles = await cur.fetchall()
            for lrole in leveled_roles:
                role = get(context.guild.roles, name=lrole[2])
                embed.add_field(
                    name="Required: {}".format(lrole[-1]),
                    value=role.mention,
                    inline=False,
                )
            await context.send(embed=embed)

    @leveled_roles.command()
    @commands.guild_only()
    @commands.has_any_role("Mods")
    async def refresh_cache(self, context):
        """Command shortcut to run self.startup_load"""
        self.startup_load.start()
        await context.send("Reloading leveled roles")

    @leveled_roles.command()
    @commands.guild_only()
    @commands.has_any_role("Mods")
    async def add(self, context, amount: int, role_name: str):
        """Add a seleced role to the guild.

        Parameters
        ----------
        amount: int
        The amount of XP required for the leveled role
        role_name: str
        The name of the role to be given.

        Uses
        ----
        - self.bot.pg_pool
        - INSERT_LR_SQL
        """
        if not role_name or not amount:
            await context.send(
                "Please provide an amount (number) and a role name (in quotes)"
            )
            return
        role = get(context.guild.roles, name=role_name)
        if not role:
            await context.send("Could not find role with name {}".format(role_name))

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(INSERT_LR_SQL, (context.guild.id, role_name, amount))
        await context.send("Added role {} for {} xp".format(role.mention, amount))

    @leveled_roles.command()
    @commands.guild_only()
    @commands.has_any_role("Mods")
    async def remove(self, context, role_name: str):
        """"""
        await context.send("This isnt finished yet :(")

    @leveled_roles.command()
    @commands.guild_only()
    async def convert(self, context):
        """Checks the callers roles. If the caller as a leveled role the bot offers
        to set users score to leveled roles requirement.

        Uses
        ----
        - self.bot.redis
        - REDIS_LR_KEY
        - {guild_id}:score
        """
        lr_key = REDIS_LR_KEY.format(context.guild.id)
        score_key = "{}:score".format(context.guild.id)
        user_score = await self.bot.redis.hget(
            score_key, context.author.id, encoding="utf-8"
        )
        # get users score from Redis
        user_role_set = {role.name for role in context.message.author.roles}
        # get users roles as a set
        guild_leveled_roles = await self.bot.redis.hgetall(lr_key, encoding="utf-8")
        # leveled roels as a dict {score_required: "leveled role name", ....}
        guild_lr_set = {name for name in guild_leveled_roles.values()}
        # convtert to a set of role names
        convert_role_intersect = guild_lr_set.intersection(user_role_set)
        # Intersect returns a set{} of only *common* items. So only leveled roles
        # on the user and in Redis will be returned.
        convert_role_name = next(iter(convert_role_intersect))
        # Sets work like iterators and require a startup
        convert_role = get(context.guild.roles, name=convert_role_name)
        # Finally fetch the role from the server
        convert_score = 0

        if not convert_role:
            await context.send("No role detected :(")
            return

        for score, role_name in guild_leveled_roles.items():
            if role_name == convert_role_name:
                convert_score = int(score)
        if convert_score > int(user_score):
            score_increase = "⬆️ Score will Increase!"
        else:
            score_increase = "⬇️ Score will Decrease!"

        desc = "{} react with a 👍 to convert your Tatsumaki score to {} score. If this was a mistake, react with 👎.".format(
            context.author.mention, self.bot.user.mention
        )
        embed = discord.Embed(
            title="Convert Tatsu score to {} score?".format(self.bot.user.name),
            description=desc,
            color=context.author.top_role.color,
        )
        embed.set_thumbnail(url=context.guild.icon_url)
        embed.add_field(name="Detected Role:", value="{}".format(convert_role.mention))
        embed.add_field(name="Score Affect:", value=score_increase)
        embed.set_author(name=context.author.name, icon_url=context.author.avatar_url)
        confirm_message = await context.send(embed=embed)

        def check_author_and_message(reaction, user):
            return (
                reaction.message == confirm_message and user == context.message.author
            )

        reaction, _ = await self.bot.wait_for(
            "reaction_add", timeout=60, check=check_author_and_message
        )

        if reaction.emoji == "👍":
            await self.bot.redis.hset(score_key, context.author.id, convert_score)
            await confirm_message.edit(content="Score Converted!")
        elif reaction.emoji == "👎":
            await confirm_message.edit(content="Canceled!")


def setup(bot):
    """Adds LeveledRolesPlugin to bot if redis is available

    Uses
    ----
    - bot.redis
    """
    setup_check_postgres(bot, __file__)
    setup_check_redis(bot, __file__)
    bot.add_cog(LeveledRolesPlugin(bot))
