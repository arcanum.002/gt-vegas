import json
import re

from typing import List

import discord
import httpx
from discord.ext import commands
from discord.ext import tasks

from checks import requires_postgres
from checks import setup_check_postgres
from checks import requires_redis
from checks import setup_check_redis

from settings.base import XP_IGNORED_CATEGORIES, XP_IGNORED_CHANNELS

try:
    from settings.local_settings import MERWEB_API_KEY
except ImportError:
    print(
        "Word of the Day requires settings.local_settings.MERWEB_API_KEY to be a valid API key!"
    )
    exit()


SELECT_REMAINING_WORDS = "SELECT * FROM wotd_wotd WHERE archived='f' AND (date_for is null OR date_for<NOW()) AND guild = %s ORDER BY date_for DESC OFFSET %s ROWS FETCH NEXT 25 ROWS ONLY;"
SELECT_TODAYS_WORD = "(SELECT * FROM wotd_wotd WHERE archived='f' AND (date_for is null OR date_for<NOW()) AND guild = %s ORDER BY date_for DESC) UNION SELECT * FROM wotd_wotd where date_for=NOW() AND guild = %s LIMIT 1;"
INSERT_WORD = "INSERT INTO wotd_wotd (word, author, xp_reward, multiplier_reward, date_added, date_for, date_found, archived, guild) VALUES (%s, %s, %s, %s, NOW(), %s, null, False, %s);"
DEL_WORD = "DELETE FROM wotd_wotd WHERE id=%s AND guild=%s;"
ARCHIVE_WORD = "UPDATE wotd_wotd SET archived='t' WHERE word=%s AND guild=%s;"


WOTD_KEY = "{}:wotd"

DICTIONARY_URL = "https://www.merriam-webster.com/dictionary/{}"
DICTIONARY_API = "https://www.dictionaryapi.com/api/v3/references/collegiate/json/{word}?key={api_key}"
DICTIONARY_IMG = "https://merriam-webster.com/assets/mw/static/social-media-share/mw-logo-245x245@1x.png"


ANNOUNCMENT_CHAN = "bot-testing"


class WotDPlugin(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.startup_load.start()

    async def _de_tokenize(self, definition_text: List[str]) -> str:
        definition_string = re.sub(r"\{bc\}", "**:** ", definition_text)
        definition_string = re.sub(
            r"\{[adiet]{,2}_link\|(\w+)\}",
            r"[\1](https://www.merriam-webster.com/dictionary/\1)",
            definition_string,
        )
        definition_string = re.sub(
            r"\{[sd]xt?\|([\w\s]+)\|(\w+:?\d?)?\|\}",
            r"[\1](https://www.merriam-webster.com/dictionary/\1)",
            definition_string,
        )
        definition_string = re.sub(
            r"\{it\}([\w+\s,\-\*:\d]+)\{\/it\}", r"*\1* ", definition_string
        )
        definition_string = re.sub(
            r"\{b\}([\w+\s,\-\*:\d]+)\{\/b\}", r"**\1** ", definition_string
        )
        definition_string = re.sub(r"\{[lr]dquo\}", r"\"", definition_string)

        return definition_string

    @tasks.loop(minutes=1.0, count=1)
    async def startup_load(self):
        if not hasattr(self.bot, "redis"):
            return
        if not hasattr(self.bot, "pg_pool"):
            return
        await self.bot.wait_until_ready()

        for guild in self.bot.guilds:
            key = WOTD_KEY.format(guild.id)
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(
                        SELECT_TODAYS_WORD,
                        (
                            guild.id,
                            guild.id,
                        ),
                    )
                    todays_word = await cur.fetchone()
            if todays_word:
                await self.bot.redis.hset(key, "word", todays_word[1])
                await self.bot.redis.hset(
                    key, "date_added", todays_word[5].strftime("%d-%m-%Y")
                )

                if todays_word[2]:
                    await self.bot.redis.hset(key, "author", todays_word[2])
                if todays_word[3]:
                    await self.bot.redis.hset(key, "xp_reward", todays_word[3])
                if todays_word[4]:
                    await self.bot.redis.hset(key, "multi_reward", todays_word[4])

    @commands.Cog.listener()
    @commands.guild_only()
    async def on_message(self, message):
        if not hasattr(self.bot, "redis"):
            return
        if message.author.bot:
            return
        if message.channel.name in XP_IGNORED_CHANNELS:
            return
        if message.channel.category.name in XP_IGNORED_CATEGORIES:
            return

        context = await self.bot.get_context(message)
        guild = message.guild
        key = WOTD_KEY.format(guild.id)
        redis = self.bot.redis

        if not await redis.hexists(key, "word"):
            return

        wotd = await redis.hget(key, "word", encoding="utf-8")

        if wotd.lower() in message.content.lower():
            # If word in message
            if await redis.hexists(key, "xp_reward"):
                score = await redis.hget(key, "xp_reward", encoding="utf-8")
                add_score_cmd = self.bot.get_command("xp add")
                await context.invoke(add_score_cmd, message.author, int(score))
            if await redis.hexists(key, "multi_reward"):
                multi = await redis.hget(key, "multi_reward", encoding="utf-8")
                multi_score_cmd = self.bot.get_command("xp multiplier")
                await context.invoke(
                    multi_score_cmd, message.author, float(multi), "1d"
                )
            wotd_embed_cmd = self.bot.get_command("wotd word_embed")
            await context.invoke(wotd_embed_cmd, "gt-community-social", wotd)

            # Change to next wotd
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(ARCHIVE_WORD, (wotd, guild.id))
            await self.bot.redis.delete(key)
            await message.channel.send(
                "{} message contained wotd - {}!".format(message.author.mention, wotd)
            )

    @commands.group()
    @commands.guild_only()
    @commands.has_any_role("Can QotD")
    async def wotd(self, context):
        if context.invoked_subcommand is None:
            pass

    @wotd.command()
    async def word_embed(self, context, channel: discord.TextChannel, word: str):
        if not word:
            await context.send(":(")
            return

        channel = await commands.TextChannelConverter().convert(
            ctx=context, argument=channel
        )

        # Build and post embed
        async with httpx.AsyncClient() as client:
            response = await client.get(
                DICTIONARY_API.format(word=word, api_key=MERWEB_API_KEY)
            )
        print(DICTIONARY_API.format(word=word, api_key=MERWEB_API_KEY))
        json_res = json.loads(response.content)
        import pprint

        print("*" * 70)
        pprint.pprint(json_res)
        definitions = json_res[0]["def"][0]["sseq"]
        function_label = json_res[0]["fl"]
        word_hw = json_res[0]["hwi"]["hw"].replace("*", "·")
        et = await self._de_tokenize(json_res[0]["et"][0][1])
        shortdef = json_res[0]["shortdef"][0]
        # stems = ", ".join(json_res[0]["meta"]["stems"])
        # offensive = json_res[0]["meta"]["offensive"]
        word_link_fmt = DICTIONARY_URL.format(word.replace(" ", ""))
        word_link = "[Link to {word}]({link})".format(word=word, link=word_link_fmt)
        desc = "*{function_label}*\n\n **{head_word}**\n\n **Etymology:** {etymology}\n\n **Definition:** {shortdef}\n\n{link}".format(
            head_word=word_hw,
            etymology=et,
            shortdef=shortdef,
            function_label=function_label,
            link=word_link,
        )
        footer_text = "Definitions provided by Merriam Webster"

        embed = discord.Embed(
            title="Today's Word: {}".format(word),
            description=desc,
            color=context.message.author.top_role.color,
        )
        embed.set_author(
            name=json_res[0]["hwi"]["prs"][0]["mw"],
            url=DICTIONARY_URL.format(word.replace(" ", "")),
            icon_url=DICTIONARY_IMG,
        )
        embed.set_thumbnail(url=DICTIONARY_IMG)
        embed.set_footer(text=footer_text, icon_url=DICTIONARY_IMG)

        for sense in definitions:
            for definition in sense:
                definition = definition[1]
                if isinstance(definition, list):
                    definition = definition[1][1]
                definition_string = "\n".join(definition["dt"][0][1:])
                definition_text = await self._de_tokenize(definition_string)
                if definition.get("sn", None):
                    definition_num = "Definition {}".format(definition["sn"])
                    embed.add_field(
                        name=definition_num, value=definition_text, inline=False
                    )
        await channel.send(embed=embed)

    @wotd.command()
    @requires_redis()
    @requires_postgres()
    async def setup(self, context):
        """Selects a word for the wotd to check for!"""
        key = WOTD_KEY.format(context.guild.id)
        if await self.bot.redis.hexists(key, "word"):
            await context.send("Theres already a word selected for today...")
            return
        await context.send("Selecting new Word of the Day")
        self.startup_load.start()
        await context.send("WotD Setup!")

    @wotd.command()
    @requires_postgres()
    async def add(
        self, context, word, author="", xp_reward="", multiplier="", date_for=None
    ):
        if not word:
            await context.send("Please provide a word!")
            return
        if author == "":
            author = context.message.author.mention

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(
                    INSERT_WORD,
                    (word, author, xp_reward, multiplier, date_for, context.guild.id),
                )
        await context.send("Word added!")

    @wotd.command()
    @requires_postgres()
    async def delete(self, context, word_id: int):
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(
                    DEL_WORD,
                    (
                        word_id,
                        context.guild.id,
                    ),
                )
        await context.send("Deleted Word: {}".format(word_id))

    @wotd.command()
    @requires_postgres()
    async def list(self, context, page=1):
        page_display = page
        page_offset = (page - 1) * 25
        desc = "This embed displays some of the remaining Words of the Day!"

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(
                    SELECT_REMAINING_WORDS,
                    (
                        context.guild.id,
                        page_offset,
                    ),
                )
                wotds = await cur.fetchall()

        embed = discord.Embed(
            title="WotD Page {}".format(page_display),
            description=desc,
            color=context.message.author.top_role.color,
        )
        embed.set_thumbnail(url=context.guild.icon_url)
        embed.set_footer(
            text="You can also pass in pages if there are more than 25 results! `v!wotd list {}`".format(
                page_display + 1
            )
        )
        for record in wotds:
            field = "*ID:* {}\n".format(record[0])
            if record[2]:
                field += "*Author:* @{}\n".format(record[2])
            if record[3]:
                field += "*XP Reward:* {}\n".format(record[3])
            if record[4]:
                field += "*Multiplier Reward:* {}\n".format(record[4])
            if record[5]:
                field += "*Date Added:* {}\n".format(record[5])
            if record[6]:
                field += "*Scheduled Date:* {}\n".format(record[6])

            embed.add_field(name=record[1], value=field, inline=False)

        await context.send(embed=embed)


def setup(bot):
    setup_check_postgres(bot, __file__)
    setup_check_redis(bot, __file__)
    bot.add_cog(WotDPlugin(bot))
