"""This module provides all functionality for the `GamesPlugin`.

"""
import discord
import random
import time
from discord import Embed
from discord.ext import commands
from discord.utils import get
from discord.ext.commands import has_any_role

from .base import BasePlugin

SLOTS_MULTIPLIERS = [0,0,0,0,0,0,0,0,0,0,0,0,1,3,5,10]
SLOTS_EMOTES = [":pleading_face:",":sob:", ":sleeping:", ":smile:",":pensive:",":face_with_monocle:", ":open_mouth:"]

class GamesPlugin(BasePlugin):
    """Plugin to handle all games in the server.
    """
    def __init__(self, bot):
        """Basic plugin init

        Parameters
        ----------
        bot: discord.ext.commands.Bot
        """
        self.bot = bot

    def check_for_role(*roles):
        """Check that returns a boolean
        
        Helper function that returns true if the user has none of the roles in the list, used for no roles

        Parameters
        ----------
        *roles: list(str)
            A list of strings that correspond to discord.Role objects.

        Returns
        -------
            boolean: True or False
                If the user **doesn't** have any of the roles listed, the method returns True.
                If the user has any role listed, the method returns False.
        """
        async def predicate(context):
            if all(discord.utils.get(context.guild.roles, name=role) not in context.author.roles for role in roles):
                return True
            await context.send("You can't do Game commands because you have the No Games role. :(")
        return commands.check(predicate)



    @commands.group(description="Fun stuff for you to do!",
                    case_insensitive=True,
                    aliases=['g',])
    @check_for_role("No Games")
    async def games(self, context):
        if context.invoked_subcommand is None:
            await context.send('Please run a subcommand!')

    @games.command(description="Play slots!", case_insensitive=True)
    async def slots(self, ctx, credits=None):
        """Used to play slots and gamble credits

            Parameters
            ----------
            ctx: discord.Context
                A discordpy context object.
            credits: str
                A string either containing the number of credits the player wants to gamble.

            Notes
            -----
            There still needs to be a limit added as well as a case for when zero credits are used
            """
        author = ctx.author.name
        if credits is None:
            await ctx.send("Enter an actual number of credits you nerd")
        if credits.isdigit():
            credits = int(credits)
        
            slots_credits = SLOTS_MULTIPLIERS[random.randint(0, 15)] * credits
            slots_emote = SLOTS_EMOTES[random.randint(0, 6)]
            if slots_credits > 0:
                msg = await ctx.send(content = "**[  :slot_machine: | SLOTS ]**\n------------------\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + "\n\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] +" **<**\n\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)])
                time.sleep(1)
                await msg.edit(content = "**[  :slot_machine: | SLOTS ]**\n------------------\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + "\n\n" + slots_emote + " : " + slots_emote + " : " + slots_emote +" **<**\n\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)])
                await ctx.send(author + " used "+ str(credits) + " credit(s) and won " + str(slots_credits) + " credits!")
            else:
                msg = await ctx.send(content = "**[  :slot_machine: | SLOTS ]**\n------------------\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + "\n\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] +" **<**\n\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)])
                time.sleep(1)
                new_slots = random.sample(range(6), 3) #Gets 3 emotes that aren't the same
                await msg.edit(content = "**[  :slot_machine: | SLOTS ]**\n------------------\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + "\n\n" + SLOTS_EMOTES[new_slots[0]] + " : " + SLOTS_EMOTES[new_slots[1]] + " : " + SLOTS_EMOTES[new_slots[2]] +" **<**\n\n" + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)] + " : " + SLOTS_EMOTES[random.randint(0, 6)])
                await ctx.send("**" + author +"** used "+ str(credits) + " credit(s) and lost everything :(")
        else:
            await ctx.send("Make sure you enter a proper number of credits!")


def setup(bot):
    bot.add_cog(GamesPlugin(bot))
