"""This module provides all functionality for the `AmongUsPlugin`.

Attributes
----------
CHAN1_NAME: str
    The name of the first channel out of the two lobbies that this plugin runs on.
CHAN2_NAME: str
    The name of the second channel out of the two lobbies that this plugin runs on.
"""
import discord
from discord import Embed
from discord.ext import commands
from discord.utils import get
from discord.ext.commands import has_any_role

from .base import BasePlugin

CHAN1_NAME = "among-us"
CHAN2_NAME = "among-them"


class AmongUsPlugin(BasePlugin):
    """Plugin to handle all Among Us activity in the server. Uses two separate lobbies to handle traffic of up to 20 users."""

    def __init__(self, bot):
        """Basic plugin init

        Parameters
        ----------
        bot: discord.ext.commands.Bot
        """
        self.bot = bot
        self.host1 = None
        self.players1 = 0
        self.host2 = None
        self.players2 = 0

    def check_for_role(*roles):
        """Check that returns a boolean

        Helper function that returns true if the user has none of the roles in the list, used for no roles

        Parameters
        ----------
        *roles: list(str)
            A list of strings that correspond to discord.Role objects.

        Returns
        -------
            boolean: True or False
                If the user **doesn't** have any of the roles listed, the method returns True.
                If the user has any role listed, the method returns False.
        """

        async def predicate(context):
            if all(
                discord.utils.get(context.guild.roles, name=role)
                not in context.author.roles
                for role in roles
            ):
                return True
            await context.send(
                "You can't do Among Us commands because you have the No Among Us role. :("
            )

        return commands.check(predicate)

    @commands.Cog.listener()
    async def on_message(self, message):
        """Method that checks for every message.

        Parameters
        ----------
        message: discord.Message
            A discordpy message object.

        Notes
        -----
        Checks if the user is the host of the room,
        if the message is in a channel and starts with "code:", it is automatically pinned
        """
        if (message.author == self.host1 or message.author == self.host2) and (
            message.channel.name == CHAN1_NAME or message.channel.name == CHAN2_NAME
        ):
            if message.content.lower().startswith("code:"):
                await message.pin()

    @commands.group(
        description="Among Us",
        case_insensitive=True,
        aliases=[
            "au",
        ],
    )
    @check_for_role("No Among Us")
    async def among_us(self, context):
        """Group method to group all among us commands.

        Parameters
        ----------
        context: discord.Context
            A discordpy context object.

        Note
        ----
        Checks for No Among Us role on the user.

        - Uses `check_for_role` helper method
        """
        if context.invoked_subcommand is None:
            await context.send("Please run a subcommand!")

    @among_us.command(description="claim a room to host", case_insensitive=True)
    async def host(self, context, claimNum=None):
        """Used for a user to claim one of the Among Us lobbies to host.

        Parameters
        ----------
        context: discord.Context
            A discordpy context object.
        claimNum: str
            A string either containing 1 or 2 which specifies the lobby the host wants to claim.

        Notes
        -----
        Sends a discord.Message stating which room the host claimed unless there has been an error.
        Gives the host the role corresponding the the game they're hosting and sets a host variable to the host.
        """
        role1 = discord.utils.get(context.guild.roles, name="Among Us Game 1")
        role2 = discord.utils.get(context.guild.roles, name="Among Us Game 2")
        ping_role = discord.utils.get(
            context.guild.roles, name="Among Us Game Notification"
        )
        if claimNum == "1" and self.host1 == None and not role2 in context.author.roles:
            await context.send(
                '{ping_role}, {host} has claimed Among Us Game 1 to host.\nUse"v!au join {host}" to join their game.'.format(
                    ping_role=ping_role.mention, host=context.author.mention
                )
            )
            self.host1 = context.author
            await context.author.add_roles(role1)
            self.players1 += 1

        elif (
            claimNum == "2" and self.host2 == None and not role1 in context.author.roles
        ):
            await context.send(
                '{ping_role}, {host} has claimed Among Us Game 2 to host.\nUse"v!au join {host}" to join their game.'.format(
                    ping_role=ping_role.mention, host=context.author.mention
                )
            )
            self.host2 = context.author
            await context.author.add_roles(role2)
            self.players2 += 1

        else:
            await context.send(
                "You must pick an empty room to host, either room 1 or room 2. You also can't host both rooms."
            )

    @among_us.command(description="show active rooms", case_insensitive=True)
    async def active(self, context):
        """Used to show the currently active games on the GTD server.

        Parameters
        ----------
        context: discord.Context
            A discordpy context object.

        Notes
        -----
        Sends a `discord.Embed` with the currently active games, the hosts of the games, and the amount of players of the games.
        """
        role1 = discord.utils.get(context.guild.roles, name="Among Us Game 1")
        role2 = discord.utils.get(context.guild.roles, name="Among Us Game 2")
        players1 = []
        players2 = []
        host1 = "None"
        host2 = "None"

        if role1.members:
            for member in role1.members:
                players1.append(member.mention + "\n")
            players1 = "".join(players1)
            host1 = self.host1.mention
        else:
            players1 = "None"
        if role2.members:
            for member in role2.members:
                players2.append(member.mention + "\n")
            players2 = "".join(players2)
            host2 = self.host2.mention
        else:
            players2 = "None"

        embed_dict = {
            "title": "Current Active Among Us Games",
            "color": context.author.top_role.color.value,
            "fields": [
                {
                    "name": "Game 1",
                    "value": "Host: {host1}\n# of Players: {numplayers1}\nPlayers: {players1}".format(
                        host1=host1, numplayers1=self.players1, players1=players1
                    ),
                    "inline": True,
                },
                {
                    "name": "Game 2",
                    "value": "Host: {host1}\n# of Players: {numplayers1}\nPlayers: {players1}".format(
                        host1=host2, numplayers1=self.players2, players1=players2
                    ),
                    "inline": True,
                },
            ],
        }

        await context.send(embed=discord.Embed.from_dict(embed_dict))

    @among_us.command(description="join a room", case_insensitive=True)
    async def join(self, context):
        """Used to allow a user to join a currently hosted lobby.

        Parameters
        ----------
        context: discord.Context
            A discordpy context object.

        Notes
        -----
        A discord.Mention object is not a parameter, but is necessary for this command to be successful.
        If all necessary requirements have been met, the necessary role is added to the user
        and the host is notified that someone has joined their lobby.
        """
        role1 = discord.utils.get(context.guild.roles, name="Among Us Game 1")
        role2 = discord.utils.get(context.guild.roles, name="Among Us Game 2")
        chan1 = get(context.guild.text_channels, name=CHAN1_NAME)
        chan2 = get(context.guild.text_channels, name=CHAN2_NAME)
        if context.message.mentions:
            if (
                self.host1 != None
                and self.host1 != context.author
                and context.message.mentions[0] == self.host1
            ):
                if self.players1 <= 10:
                    await context.author.add_roles(role1)
                    self.players1 += 1
                    await chan1.send(
                        "{host}, {player} has joined your lobby.".format(
                            host=self.host1.mention, player=context.author.mention
                        )
                    )
                else:
                    await context.send(
                        "There is a limit of 10 players to a game. Try again later."
                    )
            elif (
                self.host2 != None
                and self.host2 != context.author
                and context.message.mentions[0] == self.host2
            ):
                if self.players2 <= 10:
                    await context.author.add_roles(role2)
                    self.players2 += 1
                    await chan2.send(
                        "{host}, {player} has joined your lobby.".format(
                            host=self.host2.mention, player=context.author.mention
                        )
                    )
                else:
                    await context.send(
                        "There is a limit of 10 players to a game. Try again later."
                    )
            else:
                await context.send(
                    "You must ping the host of a room that you aren't hosting."
                )
        else:
            await context.send("You must ping the host of a room.")

    @among_us.command(description="leave a room", case_insensitive=True)
    @has_any_role("Among Us Game 1", "Among Us Game 2")
    async def leave(self, context):
        """Used to allow a member to leave a lobby that they're currently in.

        Parameters
        ----------
        context: discord.Context
            A discordpy context object.

        Notes:
        Checks to make sure the user is in a room, then removes them and notifies the host that this person has left.
        If the command user is the host, they must bequeath hostship to someone else before leaving.

        - Uses has_any_role
        """
        role1 = discord.utils.get(context.guild.roles, name="Among Us Game 1")
        role2 = discord.utils.get(context.guild.roles, name="Among Us Game 2")
        chan1 = get(context.guild.text_channels, name=CHAN1_NAME)
        chan2 = get(context.guild.text_channels, name=CHAN2_NAME)

        if context.author == self.host1:
            if self.players1 == 1:
                self.host1 = None
            elif (
                context.message.mentions
                and role1 in context.message.mentions[0].roles
                and not context.message.mentions[0] == context.author
            ):
                self.host1 = context.message.mentions[0]
                await chan1.send(
                    "{player} has left game 1, {host} is now the host.".format(
                        player=context.author.mention,
                        host=context.message.mentions[0].mention,
                    )
                )
            else:
                await context.send(
                    "Ping someone in the game after ve!au leave to bequeath your hostship to."
                )
                return
        if context.author == self.host2:
            if self.players2 == 1:
                self.host2 = None
            elif (
                context.message.mentions
                and role2 in context.message.mentions[0].roles
                and not context.message.mentions[0] == context.author
            ):
                self.host2 = context.message.mentions[0]
                await chan2.send(
                    "{player} has left game 2, {host} is now the host.".format(
                        player=context.author.mention,
                        host=context.message.mentions[0].mention,
                    )
                )
            else:
                await context.send(
                    "Ping someone in the game after ve!au leave to bequeath your hostship to."
                )
                return
        if role1 in context.author.roles:
            self.players1 -= 1
            await chan1.send("{} has left game 1.".format(context.author.mention))
            await context.author.remove_roles(role1)
            if self.players1 == 0:
                pins = await chan1.pins()
                for message in pins:
                    if message.content.lower().startswith("code:"):
                        await message.unpin()
        elif role2 in context.author.roles:
            self.players2 -= 1
            await chan2.send("{} has left game 2.".format(context.author.mention))
            await context.author.remove_roles(role2)
            if self.players2 == 0:
                pins = await chan2.pins()
                for message in pins:
                    if message.content.lower().startswith("code:"):
                        await message.unpin()
        else:
            await context.send(
                "You can't leave a room unless you're in one first, right?"
            )

    @among_us.command(description="kick someone from a room", case_insensitive=True)
    @has_any_role("Server Glue", "Disaster Director", "Mods")
    async def kick(self, context):
        """Command for any Server Glue, Disaster Director, or Mod to kick a user from an among us lobby.

        Parameters
        ----------
        context: discord.Context
            A discordpy context object.

        Notes
        -----
        A discord.Mention object is not a parameter, but is necessary for this command to be successful.
        Checks if the user meets the necessary requirements, then removes the pinged user.
        If the pinged user is the host, the command user will have to specify a new host for the lobby.

        - Uses has_any_role
        """
        role1 = discord.utils.get(context.guild.roles, name="Among Us Game 1")
        role2 = discord.utils.get(context.guild.roles, name="Among Us Game 2")
        if context.message.mentions:
            if role1 in context.message.mentions[0].roles:
                if context.message.mentions[0] == self.host1:
                    if self.players1 == 1:
                        self.host1 = None
                        if self.players1 == 0:
                            pins = await chan1.pins()
                            for message in pins:
                                if message.content.lower().startswith("code:"):
                                    await message.unpin()
                    elif len(context.message.mentions) == 2:
                        if (
                            role1 in context.message.mentions[1].roles
                            and context.message.mentions[0]
                            != context.message.mentions[1]
                        ):
                            await chan1.send(
                                "Hostship has been moved to {host}, {kicked} has been kicked.".format(
                                    host=context.message.mentions[1].mention,
                                    kicked=context.message.mentions[0].mention,
                                )
                            )
                            self.host1 = context.message.mentions[1]
                            return
                        else:
                            await context.send(
                                "You have to ping a person in your room to give hostship to."
                            )
                    else:
                        await context.send(
                            "You have to ping a person to give hostship to after the kicked person."
                        )
                        return
                else:
                    await context.send(
                        "{kicked} has been kicked.".format(
                            kicked=context.message.mentions[0].mention
                        )
                    )
                await context.message.mentions[0].remove_roles(role1)
                self.players1 -= 1
            elif role2 in context.message.mentions[0].roles:
                if context.message.mentions[0] == self.host2:
                    if self.players2 == 1:
                        self.host2 = None
                        if self.players2 == 0:
                            pins = await chan2.pins()
                            for message in pins:
                                if message.content.lower().startswith("code:"):
                                    await message.unpin()
                    elif len(context.message.mentions) == 2:
                        if (
                            role2 in context.message.mentions[1].roles
                            and context.message.mentions[0]
                            != context.message.mentions[1]
                        ):
                            await chan2.send(
                                "Hostship has been moved to {host}, {kicked} has been kicked.".format(
                                    host=context.message.mentions[1].mention,
                                    kicked=context.message.mentions[0].mention,
                                )
                            )
                            self.host2 = context.message.mentions[1]
                            return
                        else:
                            await context.send(
                                "You have to ping a person in your room to give hostship to."
                            )
                    else:
                        await context.send(
                            "You have to ping a person to give hostship to after the kicked person."
                        )
                        return
                else:
                    await context.send(
                        "{kicked} has been kicked.".format(
                            kicked=context.message.mentions[0].mention
                        )
                    )
                await context.message.mentions[0].remove_roles(role2)
                self.players2 -= 1
            else:
                await context.send(
                    "The person you tried to kick isn't currently playing among us."
                )
        else:
            await context.send("You have to ping someone to kick.")

    @among_us.command(description="show hosts of the rooms", case_insensitive=True)
    @has_any_role("Server Glue", "Disaster Director", "Mods")
    async def show_hosts(self, context):
        """Helper command to show the current hosts of a lobby.

        Parameters
        ----------
        context: discord.Context
            A discordpy context object.

        Notes
        -----
        Sends a message containing the two hosts of the current lobbies.

        - Uses has_any_role
        """
        await context.send(
            "self.host1 = {host1}\nself.host2 = {host2}".format(
                host1=self.host1, host2=self.host2
            )
        )


def setup(bot):
    bot.add_cog(AmongUsPlugin(bot))
