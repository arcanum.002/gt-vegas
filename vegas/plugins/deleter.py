import datetime

import discord
from discord.ext import commands
from discord.ext import tasks
from discord.utils import get
from discord.ext.commands import has_any_role

from discord import Embed

from settings.base import COLORS
from settings.base import NO_CHANNEL_MESSAGE
from settings.local_settings import DEBUG
from utils import to_relative_delta


COLOR = COLORS["ban"]


def is_pinned(message):
    return not message.pinned


class DeleterPlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._bot_spam_started = None
        self._bot_spam_completed = None
        self._get_access_started = None
        self._get_access_completed = None
        self.autodeleter_bot_spam_loop.start()
        self.autodeleter_get_access_loop.start()

    @tasks.loop(minutes=5.0)
    async def autodeleter_bot_spam_loop(self):
        await self.bot.wait_until_ready()
        for guild in self.bot.guilds:
            try:
                bot_spam_chan = get(guild.text_channels, name="bot-spam")
                self._bot_spam_started = datetime.datetime.now()
                if bot_spam_chan:
                    await bot_spam_chan.purge(
                        limit=200, check=is_pinned, bulk=False
                    )
                self._bot_spam_completed = datetime.datetime.now()
            except (discord.Forbidden, discord.HTTPException):
                no_bot_spam = NO_CHANNEL_MESSAGE.format(
                    guild.name, "bot-spam", self.bot.user.mention
                )
                if DEBUG:
                    print(no_bot_spam)
                else:
                    await self.bot.owner.send(no_bot_spam)

    @tasks.loop(minutes=5.0)
    async def autodeleter_get_access_loop(self):
        await self.bot.wait_until_ready()
        for guild in self.bot.guilds:
            try:

                get_access_chan = get(guild.text_channels, name="get-access")
                self._get_access_started = datetime.datetime.now()
                if get_access_chan:
                    await get_access_chan.purge(
                        limit=200, check=is_pinned, bulk=True
                    )
                self._get_access_completed = datetime.datetime.now()
            except (discord.Forbidden, discord.HTTPException):
                no_get_access = NO_CHANNEL_MESSAGE.format(
                    guild.name, "get-access", self.bot.user.mention
                )
                if DEBUG:
                    print(no_get_access)
                else:
                    await self.bot.owner.send(no_get_access)

    @commands.command(
        name="last_auto",
        description="Deletes so many messages",
        aliases=[
            "adl",
        ],
    )
    @has_any_role("Can Delete")
    async def last_auto_loop_delete(self, context):
        embed = Embed(
            description="Stats on when the last Deleter auto loop ran!", color=COLOR
        )
        bot_spam_started = to_relative_delta(self._bot_spam_started)
        bot_spam_completed = to_relative_delta(self._bot_spam_completed)
        get_access_started = to_relative_delta(self._get_access_started)
        get_access_completed = to_relative_delta(self._get_access_completed)
        bot_spam_value = "*Started:* {}\n*Ended:* {}".format(
            bot_spam_started, bot_spam_completed
        )
        get_access_value = "*Started:* {}\n*Ended:* {}".format(
            get_access_started, get_access_completed
        )

        embed.set_author(
            name=self.bot.user.name + " Auto Deleter Loop!",
            icon_url=str(context.guild.icon_url),
        )
        embed.add_field(name="Bot Spam Loop:", value=bot_spam_value, inline=False)
        embed.add_field(name="Get Access Loop:", value=get_access_value, inline=False)
        await context.send(embed=embed)

    @commands.command(
        name="del_reload",
        description="Deletes so many messages",
        aliases=[
            "dr",
        ],
    )
    @has_any_role("Can Delete")
    async def del_reload(self, context):
        try:
            self.bot.unload_extension("plugins.deleter")
            self.bot.load_extension("plugins.deleter")
        except Exception as e:
            await context.send("{} {}".format(type(e).__name__, e))
        else:
            await context.send(":thumbsup:")

    @commands.command(
        name="delete",
        description="Deletes so many messages",
        aliases=[
            "d",
            "clear",
            "del",
        ],
    )
    @has_any_role("Can Delete")
    async def delete(self, context, to_del=50, keep_pins=True):
        def is_pinned(message):
            if not bool(keep_pins):
                return True
            return not message.pinned

        deleted = await context.message.channel.purge(
            limit=to_del, check=is_pinned, bulk=True
        )
        embed = Embed(title="Deleted {} messages!".format(len(deleted)), color=COLOR)
        confirm = await context.send(embed=embed)
        await confirm.delete(delay=5)


def setup(bot):
    bot.add_cog(DeleterPlugin(bot))
