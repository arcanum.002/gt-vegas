from discord.ext import commands
import re

from .base import BasePlugin

pattern = re.compile(r"question:(?=\s*\S+)", re.I)


class autoreplyPlugin(BasePlugin):
    @commands.Cog.listener()
    @commands.guild_only()
    async def on_message(self, message):
        if "picrew" in message.channel.name:
            if message.content.lower().startswith("https://picrew.me/image_maker/"):
                await message.pin()

        elif "mod-vc" in message.channel.name:
            if re.match(pattern, message.content):
                await message.pin()

        elif "approved-vc" in message.channel.name:
            if re.match(pattern, message.content):
                await message.pin()


def setup(bot):
    bot.add_cog(autoreplyPlugin(bot))
