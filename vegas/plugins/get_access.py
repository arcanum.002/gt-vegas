import discord
from discord.ext import commands
from discord.utils import get


GET_ACCESS_FILTER = [
    "ive read the rule",
    "i've read the rule",
    "i have read the rule",
    "i read the rules",
    "i-read-the-rule",
]

GA_ID = 498298844392718346


class GetAccessPlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(hidden=True, case_insensitive=True)
    async def i_will_follow_the_rules(self, context):
        await context.message.delete()
        role = get(context.guild.roles, name="Haven't Read the Rules")
        await context.send(
            context.author.mention + ", thanks for reading the rules! Enjoy the server!"
        )
        await context.message.author.remove_roles(
            role, reason="User {} agreed to the Rules".format(context.message.author)
        )

    @commands.Cog.listener()
    async def on_message(self, message):
        if isinstance(message.channel, discord.DMChannel):
            return

        rules = get(message.guild.text_channels, name="server-rules")
        amention = message.author.mention
        m = message.content.lower()
        mo = message.content
        ga_channel = self.bot.get_channel(GA_ID)
        dds = get(message.guild.roles, name="Disaster Director")
        bots = get(message.guild.roles, name="Bots")
        mods = get(message.guild.roles, name="Mods")
        ping_list = dds.members + bots.members
        role_list = [
            dds,
            bots,
            mods,
        ]

        if message.channel.id == GA_ID:

            if message.author.id not in (691420795473494046, 376171158497918976):

                out = f"{amention}, sorry there is no talking in this channel! Please read {rules.mention} to get access to the rest of the server first!"
                if any(word in m.lower() for word in GET_ACCESS_FILTER):
                    out = f"{amention} then follow the instructions"
                elif any(mention in message.mentions for mention in ping_list) or any(
                    role in message.role_mentions for role in role_list
                ):
                    out = f"{amention}, please do not ping anyone in this channel. Read the pinned message in this channel and you should know how to unlock the server."
                elif "send" in m or m == "v!i":
                    out = f"{amention}, please read the entire text carefully, then it should be logical what you have to do."
                elif " " in m and "i_will_follow_the_rules" in m:
                    out = f"{amention}, please make sure not to include a space!"
                elif (
                    m == "i_will_follow_the_rules"
                    or m == "v"
                    or m == "v!"
                    or "i_will_follow_the_rules in" in m
                ):
                    out = f"{amention}, please read again, carefully. There may be a linebreak on mobile so consider the entire paragraph!"
                elif "V!" in mo:
                    out = f"{amention}, please make sure to follow the exact capitalization and characters from the rules."
                elif "send v!" in m:
                    out = f"{amention}, please actually do what it says, don't just copy the entire sentence."
                elif (
                    "#get-access" in m
                    or ga_channel.mention in m
                    or "get-access" in m
                    or "get access" in m
                ):
                    out = f"{amention} You are here :x: Try reading the rules, then you should know what to do."
                elif "v!\ni" in m:
                    out = f"{amention}, please do not include a linebreak. Write everything without spaces."
                elif "v!i_will_follow_the_rules" in m:
                    return
                elif "follow" in m:
                    out = f"{amention}, please make sure you didn't forget or misspell anything. Read the entire text carefully."
                await message.delete()

                await message.channel.send(out)


def setup(bot):
    bot.add_cog(GetAccessPlugin(bot))
