import asyncio

import discord
from discord.utils import get
from discord.utils import find
from discord.ext import commands

from discord.ext.commands import has_any_role

from .base import BasePlugin


DEFAULT_TIMEOUT = 380.0

ROLES = [
    "approved",
    "mods",
    "critic",
    "expert",
    "glue",
    "notif",
    "no",
    "deadlock",
    "aster",
]


class EmbedPlugin(BasePlugin):
    @commands.group(
        description="Embed Builder for Approveds!",
        case_insensitive=True,
        aliases=[
            "e",
        ],
    )
    async def embed(self, context):
        pass
        # await context.send('What do you want to do with the Embed Builder?')

    @embed.command(
        name="toggle-ping",
        description="Toggle if a role can be pinged",
        brief="Toggle a role",
        aliases=[
            "toggle",
        ],
    )
    @has_any_role("Admin", "Can Embed")
    async def toggle_ping(self, context, role_name=None):
        if any(vrole.lower() in role_name.lower() for vrole in ROLES):
            role = get(context.guild.roles, name=role_name)
            rev = not (role.mentionable)
            await role.edit(mentionable=rev)
            await context.send(role_name + " Toggled!")
        else:
            await context.send("You cannot toggle that role.")

    @embed.command(
        name="interactive-edit",
        description="Edit an Embed Interactively",
        brief="Edit an Embed Interactively",
        aliases=[
            "ie",
        ],
    )
    @has_any_role("Admin", "Mods", "Can Embed")
    async def interactive_edit(self, context, message_id):
        self.last_embed_id = message_id
        for channel in context.message.guild.text_channels:
            try:
                old_embed_message = await channel.fetch_message(message_id)
                break
            except discord.NotFound:
                pass
        old_embed = old_embed_message.embeds[0]
        embed_dict = old_embed.to_dict()
        await self.interactive_post_loop(context, embed_dict)

    @embed.command(
        name="interactive-post",
        description="Post an Embed Interactively",
        brief="Post an Embed Interactively",
        aliases=[
            "ip",
        ],
    )
    @has_any_role("Admin", "Mods", "Can Embed")
    async def interactive_post(self, context, title=None):
        embed = discord.Embed(title=title, description="Description")
        embed_dict = embed.to_dict()
        await self.interactive_post_loop(context, embed_dict)

    async def interactive_post_loop(self, context, embed_dict):
        self.pin_embed = False
        self.embed_ping_name = None
        color = context.message.author.top_role.color.value
        embed_dict["color"] = color
        embed = discord.Embed()
        embed_message = await context.send(content="Preview:", embed=embed)

        def check_emoji_menu(reaction, user):
            return user == context.message.author

        self.embed_running = True
        while self.embed_running:
            embed = embed.from_dict(embed_dict)
            await embed_message.edit(embed=embed)

            try:
                menu = (
                    "💬 - Change Description\n"
                    "🖼️ - Change Icon\n"
                    "🎨 - Change Image\n"
                    "👣 - Change Footer\n"
                    "➕ - Add Field\n"
                    "🖊️  Change Field\n"
                    "💢 - Remove Field\n"
                    "🌈 - Change Color\n"
                    "📌 - Pin Embed on Publish\n"
                    "🏓 - Add Ping to Publish\n"
                    "🆗 - Publish to Channel\n"
                    "❌ - Cancel\n"
                )
                # '⬆️  - Update last Embed \n' \

                mapper = {
                    "❌": self.close_embed,
                    "💬": self.edit_desc,
                    "🖼️": self.edit_media,
                    "🎨": self.edit_media,
                    "👣": self.edit_footer,
                    "➕": self.add_field,
                    "🖊️": self.add_field,
                    "💢": self.remove_field,
                    "🌈": self.change_color,
                    "📌": self.set_pin,
                    "🏓": self.set_ping,
                    "🆗": self.publish,
                    # "⬆️": self.update_embed,
                    "🥟": self.dump,
                }

                def fallback(*args):
                    print("Fallback in case theres no correct input")

                await embed_message.edit(
                    content=menu + "\nPreview (react to continue!):"
                )
                reaction, user = await self.bot.wait_for(
                    "reaction_add", timeout=DEFAULT_TIMEOUT, check=check_emoji_menu
                )
                menu_item = mapper.get(reaction.emoji, fallback)
                await menu_item(context, embed_message, embed_dict, reaction.emoji)
                await embed_message.clear_reactions()

            except asyncio.TimeoutError:
                self.embed_running = False
                await embed_message.edit(
                    content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                    embed=None,
                )

    async def publish(self, context, embed_message, embed_dict, *args):
        await embed_message.edit(content="What channel would you like to publish too?")
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            embed = discord.Embed()
            if message.content.isdigit():
                channel = self.bot.get_channel(int(message.content))
            else:
                channel = find(
                    lambda chan: chan.mention == message.content,
                    context.guild.text_channels,
                )

            embed = embed.from_dict(embed_dict)
            if self.embed_ping_name:
                role = get(context.guild.roles, name=self.embed_ping_name)
                await role.edit(mentionable=True)
                if channel is not None:
                    embed_message = await channel.send(
                        content=role.mention, embed=embed
                    )
                else:
                    embed_message.edit(content="Channel not found!")
            embed_message = await channel.send(embed=embed)
            if self.pin_embed is True:
                await embed_message.pin()
            await message.delete()

    async def set_pin(self, *args):
        self.pin_embed = True

    async def close_embed(self, context, embed_message, *args):
        self.embed_running = False
        self.last_embed_id = None
        await embed_message.edit(
            content="<:CoolSnapeWithFingerGunsWowww:695120559612231771>"
        )

    async def update_embed(self, context, embed_message, embed_dict, *args):
        await context.send(self.last_embed_id)
        if self.last_embed_id:
            try:
                old_embed_message = await context.fetch_message(int(self.last_embed_id))
            except discord.NotFound:
                await embed_message.edit(
                    content="There was some discord error and I can't find the message id! Please provide a message id to update!"
                )
                try:

                    def check_from_sender(message):
                        return message.author == context.message.author

                    old_embed_message = await self.bot.wait_for(
                        "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
                    )
                except asyncio.TimeoutError:
                    await embed_message.edit(
                        content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                        embed=None,
                    )

        embed = discord.Embed()
        embed = embed.from_dict(embed_dict)
        await old_embed_message.edit(embed=embed)

    async def dump(self, context, embed_message, embed_dict, *args):
        await context.send(embed_dict)

    async def edit_media(self, context, embed_message, embed_dict, media_type, *args):
        await embed_message.edit(content="Post URL:")
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            if media_type == "🖼️":
                media_type = "thumbnail"
            elif media_type == "🎨":
                media_type = "image"

            embed_dict[media_type] = {}
            embed_dict[media_type]["url"] = message.content

            if media_type == "video":
                embed_dict[media_type]["width"] = 500
                embed_dict[media_type]["height"] = 400

            await message.delete()
            return embed_dict

    async def request_num(self, context, embed_message, embed_dict):
        await embed_message.edit(content="What field would you like to edit? (num):")
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            await message.delete()
            return int(message.content)

    async def add_field(self, context, embed_message, embed_dict, *args):
        await self.edit_field(context, embed_message, embed_dict, None)

    async def edit_field(self, context, embed_message, embed_dict, field_index=-1):
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            await embed_message.edit(content="Set Field Name:")
            field_name = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
            await embed_message.edit(content="Set Field Value:")
            field_value = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
            await embed_message.edit(content="Set Field Inline (true/false):")
            field_inline = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:

            if "fields" not in embed_dict:
                embed_dict["fields"] = list()
            if field_index is None:
                field_index = len(embed_dict["fields"])
            if field_index == -1:
                field_value = (
                    await self.request_num(context, embed_message, embed_dict) - 1
                )
            if field_index >= len(embed_dict["fields"]):
                embed_dict["fields"].insert(field_index, dict())

            embed_dict["fields"][field_index]["name"] = field_name.content
            if field_value.content:
                embed_dict["fields"][field_index]["value"] = field_value.content
            else:
                embed_dict["fields"][field_index]["value"] = field_value.mentions
            embed_dict["fields"][field_index]["inline"] = field_inline.content
            await field_name.delete()
            await field_value.delete()
            await field_inline.delete()
            return embed_dict

    async def remove_field(self, context, embed_message, embed_dict, *args):
        await embed_message.edit(
            content="What field do you want to remove? (numbers only)"
        )
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            mindex = int(message.content)
            if mindex < len(embed_dict["fields"]):
                del embed_dict["fields"][int(message.content)]
            await embed_message.edit(
                content="I don't think there are that many fields on this embed..."
            )
            await message.delete()
            return embed_dict

    async def edit_footer(self, context, embed_message, embed_dict, *args):
        await embed_message.edit(content="Set Footer Text:")
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            if "footer" not in embed_dict:
                embed_dict["footer"] = dict()
            embed_dict["footer"]["text"] = message.content
            await message.delete()
            return embed_dict

    async def edit_desc(self, context, embed_message, embed_dict, *args):
        await embed_message.edit(content="Set description:")
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            embed_dict["description"] = message.content
            await message.delete()
            return embed_dict

    async def set_ping(self, context, embed_message, *args):
        await embed_message.edit(content="Set Ping for Publish:")
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            ping = message.content
            if ping:
                if any(vrole.lower() in ping.lower() for vrole in ROLES):
                    self.embed_ping_name = message.content
                else:
                    await embed_message.edit(
                        content="It looks like you can't ping that role."
                    )
            await message.delete()

    async def change_color(self, context, embed_message, embed_dict, *args):
        await embed_message.edit(content="Set Color (hex without #):")
        try:

            def check_from_sender(message):
                return message.author == context.message.author

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            embed_dict["color"] = int(message.content, 16)
            await message.delete()
            return embed_dict

    @embed.command(
        name="post",
        description="Post an Embed",
        brief="Post an Embed",
        aliases=[
            "p",
        ],
    )
    @has_any_role("Admin", "Can Embed")
    async def post(
        self, context, title=None, description=None, contact=None, dates=None, ping=None
    ):
        color = context.message.author.top_role.color
        embed = discord.Embed(title=title, description=description, colour=color)
        embed.add_field(name="Contact", value=contact, inline=False)
        embed.add_field(name="Dates:", value=dates, inline=True)
        if context.message.attachments:
            embed.set_thumbnail(url=context.message.attachments[0].url)
        if ping:
            if any(vrole.lower() in ping.lower() for vrole in ROLES):
                role = get(context.guild.roles, name=ping)
                await role.edit(mentionable=True)
                message = await context.send(role.mention, embed=embed)
                await role.edit(mentionable=False)
            else:
                message = await context.send(
                    "Sorry, you don't have permission to ping that role!"
                )
        else:
            message = await context.send(embed=embed)
        await message.pin()


def setup(bot):
    bot.add_cog(EmbedPlugin(bot))
