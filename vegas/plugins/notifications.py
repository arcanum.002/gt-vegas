"""This Cog provides members and Disaster Director access to roles.

TIMEOUT: int
    The global asyncio wait_for timeout for this module.
GET_ROLE: str
    The SQL used to insert records into `discord_user_discordrole`.
REMOVE_ROLE: str
    The SQL used to update records into `discord_user_discordrole` about role removal.
PRONOUN_FILTER: list[str]
    A filter used to check if a role is a pronoun role.
DD_ROLE_FILTER: list[str]
    A filter to check if Disaster Directors are allowed use a role.
NOTIF_DESC: str
    A quick access constant for the notifications embed description.
"""
import datetime
import asyncio
import typing

import discord
from discord.ext import commands
from discord.ext import tasks
from discord.utils import get
from discord.ext.commands import MemberConverter
from discord.ext.commands import RoleConverter
from discord.ext.commands import TextChannelConverter

from checks import has_user_ping

from utils import process_timeout
from .base import BasePlugin

from settings.base import COLORS
from settings.base import APPROVED_ROLES
from settings.base import NOTIFICATION_DICT
from settings.base import NO_CHANNEL_MESSAGE
from settings.local_settings import DEBUG

from checks import requires_postgres
from checks import setup_check_postgres


TIMEOUT = 1

GET_ROLE = "INSERT INTO discord_user_discordrole(date_recieved, reason, date_expired, role_id, discord_user_id, removed) VALUES(%s, %s, %s, %s, %s, 'f')"

TEMP_GET_ROLE = "INSERT INTO discord_user_discordrole(date_recieved, reason, date_expired, role_id, discord_user_id, removed) VALUES(%s, %s, %s, %s, %s, 'f');"

SELECT_TEMP_TIMEOUT = "SELECT * FROM discord_user_discordrole AS r LEFT JOIN discord_user_discorduser AS u ON r.discord_user_id=u.id WHERE r.removed='f' AND date_expired<NOW();"

TIMEOUT_ROLE = "UPDATE discord_user_discordrole SET removed='t', reason=%s WHERE id=%s"

REMOVE_ROLE = "UPDATE discord_user_discordrole SET removed='t', reason=r.reason || %s, date_expired=%s FROM discord_user_discordrole AS r LEFT JOIN discord_user_discorduser AS u ON u.id=r.discord_user_id WHERE r.role_id='%s' AND u.discord_user_id='%s' AND u.discord_guild_id='%s'"

PRONOUN_FILTER = [
    "she/her",
    "he/him",
    "they/them",
    "ask pronouns",
    "ask neopronouns",
    "prefers no pronouns",
    "it/its",
]

ROLE_FILTER = [
    "notification",
    "(all)",
] + PRONOUN_FILTER

DD_ROLE_FILTER = [
    "no",
    "muted",
    "deadlock",
    "has read the pinned doc in arg",
    "haven't read the rules",
    "lamp'd",
    "straight a's!",
    "birthday",
    "official stream commentator",
    "(dd)",
] + ROLE_FILTER

NOTIF_DESC = (
    "GTD has many roles you can recieve yourself! Use this bot command "
    "to get them! Just react to this embed to get role you want! "
    "\n ❌ to close."
)


class NotificationPlugin(BasePlugin):
    """This COG provides an interface for members and DD's to interact with roles"""

    def __init__(self, bot):
        super().__init__(bot)
        self.bot = bot
        self.role_timeout_loop.start()

    @tasks.loop(minutes=5.0)
    async def role_timeout_loop(self):
        await self._role_timeout()

    @tasks.loop(count=1.0)
    async def _role_timeout_once(self):
        await self._role_timeout()

    async def _role_timeout(self):
        """Checks if an users in the DB with a temp role should have the role removed.

        Uses
        ----
        - self.bot.pg_pool
        - TIMEOUT_ROLE
        - self._generate_embed_output
        """
        await self.bot.wait_until_ready()
        print("Starting role timeout")
        if not hasattr(self.bot, "pg_pool"):
            return
        now = datetime.datetime.now()
        for guild in self.bot.guilds:
            if hasattr(guild, "text_channels") and get(
                guild.text_channels, name="role-requests"
            ):
                notif_chan = get(guild.text_channels, name="role-requests")
            else:
                no_role_requests = NO_CHANNEL_MESSAGE.format(
                    guild.name, "role-requests", self.bot.user.mention
                )
                if DEBUG:
                    print(no_role_requests)
                else:
                    await guild.owner.send(no_role_requests)
                return

            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(SELECT_TEMP_TIMEOUT)
                    remove_roles = await cur.fetchall()

            for temp_role in remove_roles:
                server = self.bot.get_guild(int(temp_role[8]))
                role = server.get_role(int(temp_role[4]))
                member = server.get_member(int(temp_role[9]))
                reason = temp_role[2]
                async with self.bot.pg_pool.acquire() as uconn:
                    async with uconn.cursor() as ucur:
                        reason += ". Role Removed from timeout at " + str(now)
                        await ucur.execute(TIMEOUT_ROLE, (reason, temp_role[0]))

                await member.remove_roles(role)

                embed_dict = await self._generate_embed_output(
                    guild, "remove", role, member, "Temporary timeout"
                )
                await notif_chan.send(embed=discord.Embed.from_dict(embed_dict))

    async def _build_embed_and_roles_lists(self, context):
        """Private method that matches every role meeting ROLE_FILTER, with a custom
         emoji and creates an embed displaying all the roles.

        Paramters
        ---------
        context: discord.ext.commands.Context
            A discord context object

        Returns
        -------
        embed: discord.Embed
            An embed displaying all the roles meeting the ROLE_FILTER
        role_list: list[ list[str, discord.Reaction] ]
            A multidemnsional list using the format [ "role_name", discord.Reaction.custom_emoji]
        """
        custom_emoji_list = [
            reaction for reaction in context.guild.emojis if not reaction.animated
        ]
        embed = discord.Embed(description=NOTIF_DESC, color=COLORS["ban"])
        role_dict_copy = NOTIFICATION_DICT.copy()
        server_roles = {role.name: "" for role in context.guild.roles}
        notif_field_value = ""
        pronoun_field_value = ""
        all_field_value = ""

        role_dict = {**server_roles, **role_dict_copy}

        def filter(role_dict, notif_field_value, pronoun_field_value, all_field_value):
            selfrole_counter = (
                0  # used to loop through the custom_emoji list independantly
            )
            for role in role_dict:
                role_emoji = custom_emoji_list[selfrole_counter]
                if "notification" in role.lower():
                    if not NOTIFICATION_DICT.get(role):
                        role_dict[role] = role_emoji
                        notif_field_value += "{} {}\n".format(role_emoji, role)
                        selfrole_counter += 1
                    else:
                        notif_field_value += "{} {}\n".format(role_dict.get(role), role)

                    # Check if field is to long and reset
                    if len(notif_field_value) > 900:
                        embed.add_field(
                            name="Notification Roles:",
                            value=notif_field_value,
                            inline=False,
                        )
                        notif_field_value = ""

                if any(
                    pronoun_name.lower() in role.lower()
                    for pronoun_name in PRONOUN_FILTER
                ):
                    if not NOTIFICATION_DICT.get(role):
                        role_dict[role] = role_emoji
                        pronoun_field_value += "{} {}\n".format(role_emoji, role)
                        selfrole_counter += 1
                    else:
                        pronoun_field_value += "{} {}\n".format(
                            role_dict.get(role), role
                        )

                if "(all)" in role.lower():
                    if not NOTIFICATION_DICT.get(role):
                        role_dict[role] = role_emoji
                        all_field_value += "{} {}\n".format(role_emoji, role)
                        selfrole_counter += 1
                    else:
                        all_field_value += "{} {}\n".format(role_dict.get(role), role)
            return role_dict, notif_field_value, pronoun_field_value, all_field_value

        role_dict, notif_field_value, pronoun_field_value, all_field_value = filter(
            role_dict, notif_field_value, pronoun_field_value, all_field_value
        )

        if notif_field_value:
            embed.add_field(
                name="Notification Roles:", value=notif_field_value, inline=False
            )
        if pronoun_field_value:
            embed.add_field(
                name="Pronoun Roles:", value=pronoun_field_value, inline=False
            )
        if all_field_value:
            embed.add_field(name="Open Roles:", value=all_field_value, inline=False)

        embed.set_author(
            name=self.bot.user.name + " Self Roles!",
            icon_url=str(context.guild.icon_url),
        )

        return embed, role_dict

    async def _generate_embed_output(
        self,
        guild: discord.Guild,
        embed_type: str,
        role: discord.Role,
        user: discord.Member,
        reason: str,
    ) -> dict:
        """Private helper command to create an embed explaining how the users roles
        have been updated

        Parameters
        ----------
        context: discord.ext.commands.Context
            A discord Context object.
        embed_type: str
            A string to identify what kind of embed to make.
        role: discord.Role
            The role that has been changed.
        user: discord.Member
            The user that has been changed.
        reason: str
            The reason to display.

        Returns
        -------
        dict
            A dictionary representing the embed object.
        """
        if embed_type == "give":
            action = "Given"
            color = COLORS["role_added"]
        elif embed_type == "temp_give":
            action = "Temporarily given"
            color = COLORS["role_added"]
        elif embed_type == "join":
            action = "Joined"
            color = COLORS["role_added"]
        elif embed_type == "remove":
            action = "Removed"
            color = COLORS["role_removed"]
        elif embed_type == "leave":
            action = "Left"
            color = COLORS["role_removed"]

        return {
            "color": color,
            "author": {
                "name": "{} roles updated!".format(user.name),
                "icon_url": str(guild.icon_url),
            },
            "thumbnail": {
                "url": str(user.avatar_url),
            },
            "fields": [
                {
                    "name": "Roles {}".format(action),
                    "value": str(role.name),
                },
                {
                    "name": "Reason:",
                    "value": reason,
                },
            ],
            "footer": {
                "icon_url": str(self.bot.user.avatar_url),
                "text": "Thanks for using {}!".format(self.bot.user.name),
            },
        }

    async def _check_user_can_get_role(
        self, context: discord.ext.commands.Context, role_name: str, filter_type="gen"
    ):
        """Checks if a user is allowed to get/remove a role

        Parameters
        ----
        context: discord.ext.commands.Context
            A discord context object
        filter_type: str
            Controls the role approval filter tobe applied
        role_name: str
            The discord.Role.name to find.
        filter_type: str
            What type of filter should be applied. Defaults to 'gen' ROLE_FILTER, or 'dd' DD_ROLE_FILTER

        Returns
        -------
        None
            No role was found.
        discord.Role
            The role found by discord.Role.name.
        """
        fail_message = "You don't have permission to do that."

        if filter_type == "dd":
            filter_roles = DD_ROLE_FILTER
        elif filter_type == "gen":
            filter_roles = ROLE_FILTER

        if any(
            filter_role.lower() in role_name.lower() for filter_role in filter_roles
        ):
            role = get(context.guild.roles, name=role_name)

            if not role:
                await context.send("I cannot find {}.".format(role_name))
                return None
            return role

        elif any(arole.lower() in role_name.lower() for arole in APPROVED_ROLES):
            info_channel = get(context.guild.text_channels, name="server-info")
            help_channel = get(context.guild.text_channels, name="server-help")
            await context.send(
                "You can not add that role to yourself. Please take a couple minutes and read {}.\nIf you're still confused afterwards feel free to ask in {}.".format(
                    info_channel.mention, help_channel.mention
                )
            )
        else:
            await context.send(fail_message)
            return None

    @commands.group(
        description="Get added or removed to notification roles",
        case_insensitive=True,
        aliases=[
            "n",
            "noti",
        ],
    )
    @requires_postgres()
    @commands.guild_only()
    async def notifications(self, context):
        """Displays a discord.Embed of all roles meeting ROLE_FILTER. Anyone can react to
        recieve the role display by the custom emoji.

        Notes
        -----
        - Uses `self._build_embed_and_roles_lists`
        - Uses `TIMEOUT`
        - Uses `self._generate_embed_output`
        """
        if context.invoked_subcommand is None:
            author_roles = [role.name.lower() for role in context.message.author.roles]
            embed, role_dict = await self._build_embed_and_roles_lists(context)
            embed_message = await context.send(embed=embed)

            if "mods" not in author_roles:
                pin = False
            else:
                pin = True

            if pin:
                await embed_message.pin()

            def allow_all(reaction, user):
                return True

            while True:
                try:
                    reaction, user = await self.bot.wait_for(
                        "reaction_add", timeout=TIMEOUT, check=allow_all
                    )
                    if str(reaction.emoji) == "❌":
                        await embed_message.edit(
                            content="Thanks for using VEGAS!", embed=None
                        )
                        break

                except asyncio.TimeoutError:
                    if not pin:
                        await embed_message.edit(
                            content="**[Timeout]** Thanks for using {}".format(
                                self.bot.user.mention
                            ),
                            embed=None,
                        )
                    break

                else:
                    for role_name, emoji in role_list:
                        if str(reaction) == str(emoji):
                            toggle_role = get(context.guild.roles, name=role_name)
                    if toggle_role:
                        if toggle_role in user.roles:
                            embed_type = "leave"
                            await user.remove_roles(toggle_role)
                        else:
                            embed_type = "join"
                            await user.add_roles(toggle_role)

                        embed_dict = await self._generate_embed_output(
                            context.guild, embed_type, toggle_role, user, "Self Roles"
                        )

                        update_message = await context.send(
                            embed=discord.Embed.from_dict(embed_dict)
                        )
                        await update_message.delete(delay=5)
                        await embed_message.clear_reactions()

    @notifications.command(
        name="give",
        description="Give a notification role",
        brief="Give a notification role (Disaster Director Only)",
        aliases=[
            "g",
        ],
    )
    @has_user_ping()
    @commands.guild_only()
    @commands.has_any_role("Mods", "Disaster Director")
    async def give(
        self,
        context: discord.ext.commands.Context,
        username: discord.Member,
        echo_channel,
        role_name: str,
        *reason,
    ) -> None:
        """Allows Disaster Directoris or Mods to give users a role that meets the DD_ROLE_FILTER

        Parameters
        ----------
        context: discord.ext.commands.Context
            A discord Context object.
        username
            A members mention to lookup.
        echo_channel
            A channel to send the embed to.
        role_name: str
            The discord.Role in double quotes.
        *reason: list[str]
            The reason the user is recieving the role.
        """
        if not reason:
            await context.send("Please provide a reason!")
        if not role_name:
            await context.send("Please provide a role name!")

        reason = "**Recieved for: **" + " ".join(reason)
        user = await MemberConverter().convert(ctx=context, argument=username)
        if echo_channel and echo_channel != "-":
            echo_channel = await TextChannelConverter().convert(
                ctx=context, argument=echo_channel
            )
        else:
            echo_channel = None

        role = await self._check_user_can_get_role(context, role_name, "dd")

        if role:
            if role in user.roles:
                await context.send("{} already has that role".format(user.name))
                return

            user_id_link = await self._check_user_exists(user.id, context.guild.id)

            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(
                        GET_ROLE,
                        (datetime.datetime.now(), reason, None, role.id, user_id_link),
                    )

            await user.add_roles(
                role, reason="Roles given by {}".format(context.message.author.mention)
            )

            embed = await self._generate_embed_output(
                context.guild, "give", role, user, reason
            )

            await context.send(embed=discord.Embed.from_dict(embed))

            if echo_channel:
                await echo_channel.send(
                    content=user.mention, embed=discord.Embed.from_dict(embed)
                )

    @notifications.command(
        description="Interactively Give a notification role",
        brief="Interactivel give a notification role (Disaster Director Only)",
        case_insensitive=True,
        aliases=[
            "ig",
        ],
    )
    @commands.has_any_role("Mods", "Disaster Director")
    @requires_postgres()
    async def interactive_give(
        self, context, user=None, role=None, echo_channel=None, *reason: typing.Optional
    ):
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)
        try:
            role = await RoleConverter().convert(ctx=context, argument=user)
        except (commands.RoleNotFound, TypeError):
            await context.send(f"I couldn't find role {role} not found!")
            role = await self._collect_response(context, "", commands.RoleConverter)
        try:
            if echo_channel != "-":
                echo_channel = await TextChannelConverter().convert(
                    ctx=context, argument=echo_channel
                )
        except (commands.ChannelNotFound, TypeError):
            await context.send(f"I couldn't find channel {echo_channel} not found!")
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )
        if reason:
            reason = " ".join(reason)
        else:
            reason = await self._collect_response(context, "reason", "string")
        confirm_message = await context.send(
            f"{user.mention} will be given role {role.mention} in {echo_channel.mention} for `{reason}`"
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        give_cmd = self.bot.get_command("n g")
        if echo_channel != "-":
            await context.invoke(
                give_cmd, user.mention, echo_channel.mention, role.name, reason
            )
        else:
            await context.invoke(give_cmd, user.mention, "-", role.name, reason)

    @notifications.command(
        name="remove",
        description="Remove a notification role",
        brief="Remove a notification role (Disaster Director Only)",
        aliases=[
            "r",
        ],
    )
    @has_user_ping()
    @commands.guild_only()
    @commands.has_any_role("Mods", "Disaster Director")
    async def remove(
        self,
        context: discord.ext.commands.Context,
        user,
        echo_channel,
        role_name: str,
        *reason,
    ) -> None:
        """Allows Disaster Directoris or Mods to remove users a role that meets the DD_ROLE_FILTER

        Parameters
        ----------
        context: discord.ext.commands.Context
            A discord Context object.
        username
            A members mention to lookup.
        role_name: str
            The discord.Role in double quotes.
        *reason: list[str]
            The reason the user is recieving the role.
        """
        if not reason:
            await context.send("Please provide a reason!")
        if not role_name:
            await context.send("Please provide a role name!")

        reason = "**Removed for: **" + " ".join(reason)
        user = await MemberConverter().convert(ctx=context, argument=user)

        if echo_channel and echo_channel != "-":
            echo_channel = await TextChannelConverter().convert(
                ctx=context, argument=echo_channel
            )
        else:
            echo_channel = None

        role = await self._check_user_can_get_role(context, role_name, "dd")

        if role:
            if role not in user.roles:
                await context.send("{} does not have that role".format(user.name))
                return

            await self._check_user_exists(user.id, context.guild.id)

            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(
                        REMOVE_ROLE,
                        (
                            reason,
                            datetime.datetime.now(),
                            role.id,
                            user.id,
                            context.guild.id,
                        ),
                    )

            await user.remove_roles(
                role,
                reason="Roles removed by {}".format(context.message.author.mention),
            )

            embed = await self._generate_embed_output(
                context.guild, "remove", role, user, reason
            )

            await context.send(embed=discord.Embed.from_dict(embed))

            if echo_channel:
                await echo_channel.send(
                    content=user.mention, embed=discord.Embed.from_dict(embed)
                )

    @notifications.command(
        description="Interactively Remove a notification role",
        brief="Interactivel remove a notification role (Disaster Director Only)",
        case_insensitive=True,
        aliases=[
            "ir",
        ],
    )
    @commands.has_any_role("Mods", "Disaster Director")
    @requires_postgres()
    async def interactive_remove(
        self, context, user=None, role=None, echo_channel=None, *reason: typing.Optional
    ):
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(context, "", commands.MemberConverter)
        try:
            role = await RoleConverter().convert(ctx=context, argument=user)
        except (commands.RoleNotFound, TypeError):
            await context.send(f"I couldn't find role {role} not found!")
            role = await self._collect_response(context, "", commands.RoleConverter)
        try:
            if echo_channel != "-":
                echo_channel = await TextChannelConverter().convert(
                    ctx=context, argument=echo_channel
                )
        except (commands.ChannelNotFound, TypeError):
            await context.send(f"I couldn't find channel {echo_channel} not found!")
            echo_channel = await self._collect_response(
                context, "", commands.TextChannelConverter
            )
        if reason:
            reason = " ".join(reason)
        else:
            reason = await self._collect_response(context, "reason", "string")
        confirm_message = await context.send(
            f"{user.mention} will be removed from role {role.mention} in {echo_channel.mention} for `{reason}`"
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        remove_cmd = self.bot.get_command("n r")
        if echo_channel != "-":
            await context.invoke(
                remove_cmd, user.mention, echo_channel.mention, role.name, reason
            )
        else:
            await context.invoke(remove_cmd, user.mention, "-", role.name, reason)

    @notifications.command(
        name="temp_give",
        description="Temporarily give a notification/no role",
        brief="Temporarily give a notification role (Disaster Director Only)",
        aliases=[
            "tg",
        ],
    )
    @commands.has_any_role("Mods", "Disaster Director")
    async def temp_give(
        self,
        context: discord.ext.commands.Context,
        user: discord.Member,
        timeout: str,
        role_name: str,
        *reason,
    ) -> None:
        """Allows Mods and Disaster Directors to temporarily give a discord.Role meeting DD_ROLE_FILTER to a member.

        Paramteres
        ----------
        context: discord.Context
            The generic discord context parameter that gets passed with every command
        user: discord.Member
            The discord member to apply the role to.
        timeout: str
            The amount of time to give therole for.
            The format for `timeout` is as follows:
                - 10m = ten minutes
                - 2h = two hours
                - 7d = seven days
                - 1w = one week
                - 2M = two months
        role_name: str
            The discord.Role.name to add to the member
        reason: str
            The reason the member recieved this role. Only used for logging purposes.

        Uses
        ----
        - self.bot.pg_pool
        """
        if not reason:
            await context.send("I need a reason...")
            return
        if not role_name:
            await context.send("Please provide a role name!")
            return
        if not timeout[0].isdigit() and timeout[-1].isalpha():
            await context.send("Please provide a proper timeout!")
            return

        reason = "**Recieved for: **" + " ".join(reason)
        role = await self._check_user_can_get_role(context, role_name, "dd")
        user_id_link = await self._check_user_exists(user.id, context.guild.id)
        now = datetime.datetime.now()

        removal_time, _ = process_timeout(timeout)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(
                    TEMP_GET_ROLE, (now, reason, removal_time, role.id, user_id_link)
                )

        if role:
            if role in user.roles:
                await context.send("{} already has that role".format(user.name))
                return

            await user.add_roles(
                role, reason="Roles given by {}".format(context.message.author.mention)
            )

        embed_dict = await self._generate_embed_output(
            context.guild, "temp_give", role, user, reason
        )
        embed = discord.Embed.from_dict(embed_dict)
        embed.add_field(
            name="Removal Time:",
            value=removal_time.strftime("%b %d, %y %H:%M"),
            inline=False,
        )

        await context.send(embed=embed)

    @notifications.command(
        description="Interactively temp-give a notification role",
        brief="Interactivel temp-give a notification role (Disaster Director Only)",
        case_insensitive=True,
        aliases=[
            "itg",
        ],
    )
    @commands.has_any_role("Mods", "Disaster Director")
    @requires_postgres()
    async def interactive_temp_give(
        self, context, user=None, timeout=None, role=None, *reason: typing.Optional
    ):
        try:
            user = await MemberConverter().convert(ctx=context, argument=user)
        except (commands.MemberNotFound, TypeError):
            await context.send(f"I couldn't find member {user} not found!")
            user = await self._collect_response(
                context, "reason", commands.MemberConverter
            )
        if timeout:
            timeout = " ".join(reason)
        else:
            timeout = await self._collect_response(context, "timeout", "string")
        try:
            role = await RoleConverter().convert(ctx=context, argument=user)
        except (commands.RoleNotFound, TypeError):
            await context.send(f"I couldn't find role {role} not found!")
            role = await self._collect_response(context, "", commands.RoleConverter)
        if reason:
            reason = " ".join(reason)
        else:
            reason = await self._collect_response(context, "reason", "string")
        confirm_message = await context.send(
            f"{user.mention} will be temp-given role {role.mention} ({timeout}) for `{reason}`"
        )

        confimed = await self._confirm_with_reaction(context, confirm_message)

        if confimed:
            return

        remove_cmd = self.bot.get_command("n tg")
        await context.invoke(remove_cmd, user, timeout, role.name, reason)

    @notifications.command(
        name="auto_remove",
        description="Runs the internal timeout functions to remove temp roles",
        brief="Time out temp roles (Disaster Director Only)",
        aliases=[
            "ar",
        ],
    )
    @commands.guild_only()
    @commands.has_any_role("Mods", "Disaster Director")
    async def auto_remove(self, context):
        await context.send(
            "Checking for users with temp roles... {}".format(context.author.mention)
        )
        await self._role_timeout_once.start()
        await context.send("Complete! {}".format(context.author.mention))

    @notifications.command(
        name="join",
        description="Join a notification role",
        brief="Join a notification role",
    )
    @commands.guild_only()
    async def join(self, context: discord.ext.commands.Context, *role_name) -> None:
        """Allows any user to join a role meeting ROLE_FILTER.

        Parameters
        ----------
        context: discord.ext.commands.Context
            A discord Context object.
        role_name
            The role name to add.
        """
        if not role_name:
            await context.send("Please provide a role name!")
        if isinstance(role_name, tuple):
            role_name = " ".join(role_name)

        reason = "Requested via {}.".format(self.bot.user.name)
        user = context.message.author

        role = await self._check_user_can_get_role(context, role_name)

        if role:
            if role in user.roles:
                await context.send("{} already has that role".format(user.name))
                return

            await user.add_roles(
                role, reason="Roles given by {}".format(context.message.author.mention)
            )

            embed = await self._generate_embed_output(
                context.guild, "join", role, user, reason
            )

            await context.send(embed=discord.Embed.from_dict(embed))

    @notifications.command(
        name="leave",
        description="Leave a notification role",
        brief="Leave a notification role",
    )
    @commands.guild_only()
    async def leave(self, context: discord.ext.commands.Context, *role_name) -> None:
        """Allows any user to remove a role that meets ROLE_FILTER

        context: discord.ext.commands.Context
            A discord Context object.
        role_name
            The role name to add.
        """
        if not role_name:
            await context.send("Please provide a role name!")
        if isinstance(role_name, tuple):
            role_name = " ".join(role_name)

        reason = "Requested via {}.".format(self.bot.user.name)
        user = context.message.author

        role = await self._check_user_can_get_role(context, role_name)

        if role:
            if role not in user.roles:
                await context.send("{} does not have that role".format(user.name))
                return

            await user.remove_roles(
                role,
                reason="Roles removed by {}".format(context.message.author.mention),
            )

            embed = await self._generate_embed_output(
                context.guild, "leave", role, user, reason
            )

            await context.send(embed=discord.Embed.from_dict(embed))

    @commands.Cog.listener("on_raw_reaction_add")
    @commands.guild_only()
    async def user_role_toggle_reaction(
        self, payload: discord.RawReactionActionEvent
    ) -> None:
        """Monitors reactions in bot-spam and vegas-logging to match against the the
        `role_list` returned by `self._build_embed_and_roles_lists` and adds the
        appropriate role to the user.

        payload: discord.RawReactionActionEvent
            The payload to extract the reaction info from.
        """
        if not payload.guild_id:
            return
        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        context = await self.bot.get_context(message)
        user = context.guild.get_member(payload.user_id)

        if message.content != "" and user.id != self.bot.user.id:
            return
        if not message.pinned:
            return

        if channel.name == "bot-spam" or channel.name == "vegas-logging":
            toggle_role = None
            _, role_dict = await self._build_embed_and_roles_lists(context)
            for role in role_dict:
                if str(payload.emoji) == str(role_dict[role]):
                    toggle_role = get(context.guild.roles, name=role)
            if toggle_role:
                if toggle_role in user.roles:
                    embed_type = "leave"
                    await user.remove_roles(toggle_role, reason="self roles")
                else:
                    embed_type = "join"
                    await user.add_roles(toggle_role, reason="self roles")

                embed_dict = await self._generate_embed_output(
                    context.guild, embed_type, toggle_role, user, "Self Roles"
                )

                update_message = await channel.send(
                    embed=discord.Embed.from_dict(embed_dict)
                )
                await update_message.delete(delay=5)

            else:
                warn = await channel.send(
                    "{} I don't have a role for {}! ".format(
                        user.mention, payload.emoji
                    )
                )
                await warn.delete(delay=5)

            await message.clear_reactions()

    @commands.Cog.listener("on_raw_reaction_add")
    @commands.guild_only()
    async def self_mute_toggle(self, payload: discord.RawReactionActionEvent) -> None:
        """

        payload: discord.RawReactionActionEvent
            The payload to extract the reaction info from.
        """
        if not payload.guild_id:
            return
        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        context = await self.bot.get_context(message)
        user = context.guild.get_member(payload.user_id)

        if user.id == self.bot.user.id:
            return
        if not message.pinned:
            return
        if channel.name != "role-requets":
            return

        if channel.name == "role-requests":
            tmp_give_mute = self.bot.get_command("n tg")
            if str(payload.emoji) == "1️⃣":
                await context.invoke(
                    tmp_give_mute,
                    user,
                    "1h",
                    "Muted",
                    "Requested through role-requests",
                )
            elif str(payload.emoji) == "2️⃣":
                await context.invoke(
                    tmp_give_mute,
                    user,
                    "2h",
                    "Muted",
                    "Requested through role-requests",
                )
            elif str(payload.emoji) == "3️⃣":
                await context.invoke(
                    tmp_give_mute,
                    user,
                    "3h",
                    "Muted",
                    "Requested through role-requests",
                )
            elif str(payload.emoji) == "4️⃣":
                await context.invoke(
                    tmp_give_mute,
                    user,
                    "4h",
                    "Muted",
                    "Requested through role-requests",
                )
            elif str(payload.emoji) == "5️⃣":
                await context.invoke(
                    tmp_give_mute,
                    user,
                    "5h",
                    "Muted",
                    "Requested through role-requests",
                )
            elif str(payload.emoji) == "🔢":
                await channel.send(
                    "Checking for users with temp roles... {}".format(user.mention)
                )
                await self._role_timeout_once.start()
                await channel.send("Complete! {}".format(user.mention))
            else:
                await context.send("{} I don't recognize that :(".format(user.mention))

        await message.clear_reactions()


def setup(bot):
    setup_check_postgres(bot, __file__)
    bot.add_cog(NotificationPlugin(bot))
