import random
import discord
from discord.ext import commands
from .base import BasePlugin


class FunPlugin(BasePlugin):
    # If you want to use sub commands you must use this decorator to create a group
    @commands.command(aliases=["8ball"])
    async def _8ball(self, ctx, *, question):
        responses = [
            "Ask again when I give a shit.",
            "Yes, if you leave me alone.",
            "Coward.",
            "Well, duh.",
            "Try again when I actually care.",
            "Cannot predict now, I'm sleepy.",
            "Actually think about what you are asking me and try again.",
            "My sources say no.",
            "No...just no.",
            "`[In energy saving mode. Try again never]`",
            "Yeah, Sure.",
            "Better not tell you now.",
            "Concentrate and ask again.",
            "Don't count on it.",
            "Outlook not so good.",
            "Very doubtful.",
            "Yes.",
            "Absolutely.",
            "Whatever.",
        ]
        var = int("75c5f8", 16)
        embed = discord.Embed(
            title=":8ball: 8ball", description=random.choice(responses), color=var
        )
        await ctx.send(embed=embed)

    @_8ball.error
    async def _8ball_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(
                "You know you have to ask a question to get an answer, right?"
            )


def setup(bot):
    bot.add_cog(FunPlugin(bot))
