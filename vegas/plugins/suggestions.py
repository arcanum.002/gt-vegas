"""
SuggestionsPlugin

"""
import asyncio
from functools import partial
from typing import Union

import discord
from discord.ext import commands
from discord.utils import get

from settings.base import COLORS
from plugins.base import BasePlugin

from checks import requires_postgres
from checks import setup_check_postgres

TIMEOUT = 120.0

REQUIRED_ROLE = "Loyal Theorists"
REVIEW_CHANNEL_NAME = "approved-feedback"
SUGGESTION_CHANNEL = "suggestions-and-community-support"
SUGGESTION_INSERT = "INSERT INTO suggestions_suggestion (suggestion, attachment, submitted, discord_user_id, approved) VALUES (%s, %s, %s, %s,  %s)"
SUGGESTION_SELECT = "SELECT * FROM suggestions_suggestion WHERE discord_user_id = %s ORDER BY submitted DESC"
SUGGESTION_GET = "SELECT * FROM suggestions_suggestion WHERE id = %s"
SUGGESTION_UPDATE = "UPDATE suggestions_suggestion SET approved='t' WHERE id = %s;"

OPEN_ROLES = ["server glue", "mods"]


class SuggestionPlugin(BasePlugin):
    """SuggestionsPlugin offers a way for guild members to submit suggestions
    to be reviewed by Mods, then voted on by everyone!"""
    def __init__(self, bot):
        self.open_roles = OPEN_ROLES
        self.bot = bot

    async def _generate_suggestion_embed(
        self, submission, author, guild, submission_date, attach_url, sid=None
    ):
        embed = discord.Embed(
            title="New Suggestion", color=COLORS["ban"], description=submission
        )
        embed.set_author(
            name=author.name + author.discriminator, icon_url=author.avatar_url
        )
        embed.set_thumbnail(url=guild.icon_url)
        embed.add_field(name="Author:", value=author.mention, inline=True)
        embed.add_field(name="Submission Date:", value=submission_date, inline=True)

        if sid:
            embed.add_field(name="ID:", value=id)

        if attach_url:
            embed.set_image(url=attach_url)
        return embed

    async def _check_suggestion_type(self, context) -> str:
        check_is_dm_and_author_message = partial(
            self._check_is_dm_and_author_message_partial, context.message.author
        )
        timeout = 60.0
        try:
            while True:
                await context.send("What kind of suggestion would you like to make?")
                await context.send(
                    "(Reply with `User` to suggest a user for an approved role or `other` to suggest something else.)"
                )
                type_message = await self.bot.wait_for(
                    "message", timeout=timeout, check=check_is_dm_and_author_message
                )
                reply = type_message.content.split(" ")[0].lower()
                lower_content = type_message.content.lower()
                if reply == "user":
                    return "user"
                if reply == "other":
                    return "other"
                if lower_content in ('exit', 'x'):
                    await context.send("Cya!")
                    return
                await context.send("Sorry, I didn't understand that reply...")

        except asyncio.TimeoutError:
            await context.send(
                "Your suggestion has timed out! {} will wait "
                "60 seconds before timing out each question.".format(self.bot.name)
            )

    async def _interactive_get_user_submission(self, context, guild):
        check_is_dm_and_author_message = partial(
            self._check_is_dm_and_author_message_partial, context.message.author
        )
        try:
            while True:
                await context.send("What is the users ID you would like to suggest?")
                await context.send(
                    "https://support.discord.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-"
                )
                user_message = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                if (
                    user_message.content.lower() == "exit"
                    or user_message.content.lower() == "x"
                ):
                    await context.send("Cya!")
                    return
                if int(user_message.content):
                    user_id = int(user_message.content)
                    user = get(guild.members, id=user_id)
                    user_disc = f"{user.name}#{user.discriminator}"
                    embed = discord.Embed(
                        title=user_disc,
                        description=f"ID: {user_id}\nNickname: {user.nick}",
                    )
                    embed.set_thumbnail(url=user.avatar_url)
                    await context.send(embed=embed)
                    break
                else:
                    await context.send("I couldn't find a user with that ID!")
            while True:
                await context.send("What role would you like to suggest this user for?")
                role_message = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                if (
                    user_message.content.lower() == "exit"
                    or user_message.content.lower() == "x"
                ):
                    await context.send("Cya!")
                    return
                if not any(
                    role_filter in role_message.content.lower()
                    for role_filter in self.open_roles
                ):
                    await context.send(
                        "It doesn't look like that role is open for new members right now..."
                    )
                    await context.send("The current open roles are:")
                    oroles = ""
                    for role in self.open_roles:
                        oroles += "- {}\n".format(role)
                    await context.send(oroles)
                elif get(guild.roles, name=role_message.content):
                    role = get(guild.roles, name=role_message.content)
                    # set role
                    break
                else:
                    await context.send("I couldn't that role")
            while True:
                await context.send(
                    f"Please provide a brief explanation of {user_disc} they would be a good fit for the role"
                )
                reason = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                if len(reason.content) > 30:
                    break
            return f"**{user_disc}** for {role.name}\n**Reason:** {reason.content}"
        except asyncio.TimeoutError:
            await context.send(
                "Your suggestion has timed out! {} will wait "
                "60 seconds before timing out each question.".format(self.bot.name)
            )

    async def _interactive_get_other_submission(
        self, context
    ) -> Union[None, str]:
        check_is_dm_and_author_message = partial(
            self._check_is_dm_and_author_message_partial, context.message.author
        )
        try:
            while True:
                await context.send("Suggestion")
                await context.send("[Quick description of the suggestion, ~1 phrase]")
                suggestion = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                if len(suggestion.content) > 30:
                    break
            while True:
                await context.send("What is it?:")
                await context.send(
                    "[Thorough explanation of your suggestion, different types of ideas require different levels of explanation, for emotes/commands and recommending people for roles it isn't needed]"
                )
                long_suggestion = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                if len(long_suggestion.content) > 80:
                    break
            while True:
                await context.send("Why do we need it?")
                await context.send(
                    "[Explain what good would it do on the server, why do you want this?]"
                )
                why = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                if len(why.content) > 30:
                    break
            while True:
                await context.send("Anything else you would like to add?")
                await context.send(
                    "[Additional comments that would not fall under the other categories, if not just send 'no'.]"
                )
                additional = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                break
            return (
                f"**Suggestion:**\n {suggestion.content}\n\n"
                f"**What is it?**\n {long_suggestion.content}\n\n"
                f"**Why do we need it?**\n {why.content}\n\n"
                f"**Anything else you would like to add?**\n {additional.content}"
            )
        except asyncio.TimeoutError:
            await context.send(
                "Your suggestion has timed out! {} will wait "
                "60 seconds before timing out each question.".format(self.bot.name)
            )

    @commands.group(description="Have a suggestion?", case_insensitive=True)
    @requires_postgres()
    async def suggestion(self, context):
        """Base suggestion command for grouping"""
        if context.invoked_subcommand is None:
            await context.send("What do you want to do with Suggestions?")

    @suggestion.command(
        description="Add a suggestion for Staff Review!", aliases=["a", "+"]
    )
    @commands.dm_only()
    async def add(self, context):
        """Allows a user to add a suggestion.
        Ensures the user is in the guild.
        Ensures the user has a high enough role.

        """
        guild = await self._get_guild(context)
        author = guild.get_member(context.message.author.id)
        link_id = await self._check_user_exists(author.id, guild.id)
        submission = ""
        submission_date = context.message.created_at.strftime("%b %d %y")
        required_role = get(guild.roles, name=REQUIRED_ROLE)
        attach_url = ""

        if context.message.attachments:
            attach_url = context.message.attachments[0].url

        if not author.top_role.position >= required_role.position:
            await context.send(
                "You don't have a high enough role to add a suggestion. Please check out #server-info (https://discord.com/channels/353694095220408322/735635802960429087/735639337613656086) for more information."
            )
            return

        suggestion_type = await self._check_suggestion_type(context)

        if suggestion_type == "user":
            submission = await self._interactive_get_user_submission(context, guild)
        elif suggestion_type == "other":
            submission = await self._interactive_get_other_submission(context)

        if not submission:
            return

        if len(submission) > 2048:
            await context.send(
                "Discord can only accept 2048 characters, please send a Google doc or images to help explain."
            )

        embed = await self._generate_suggestion_embed(
            submission, author, guild, submission_date, attach_url
        )

        review_message = await context.send(
            content="React with 👍 to send or 👎 to cancel.", embed=embed
        )

        def _reaction_from_author(_, user):
            return user == context.author

        while True:
            reaction, _ = await self.bot.wait_for(
                "reaction_add", timeout=TIMEOUT, check=_reaction_from_author
            )
            if reaction.emoji == "👍":
                async with self.bot.pg_pool.acquire() as conn:
                    async with conn.cursor() as cursor:
                        # Creates the new record
                        await cursor.execute(
                            SUGGESTION_INSERT,
                            (submission, attach_url, submission_date, link_id, False),
                        )
                        await cursor.execute(SUGGESTION_SELECT, (link_id,))
                        suggestions = await cursor.fetchall()
                await context.send(
                    "Thanks for the suggestion! Your suggestion id is {}".format(
                        suggestions[0][0]
                    )
                )
                embed.add_field(name="ID:", value=suggestions[0][0])
                af_channel = get(guild.text_channels, name=REVIEW_CHANNEL_NAME)
                af_message = await af_channel.send(embed=embed)
                await af_message.pin()
                return
            if reaction.emoji == "👎":
                await review_message.edit(
                    content="Sorry to hear that. If you have any suggestions send them to us!",
                    embed=None,
                )
                return
            # await embed_message.clear_reactions()
            await context.send("Please react with 👍 or 👎", delete_after=5.0)

    @add.error
    async def add_error(self, context, error):
        """Error method for SuggestionsPlugin.add

        Checks for PrivateMessageOnly and asyncio.TimeoutError
        """
        if isinstance(error, commands.PrivateMessageOnly):
            await context.send(
                "You can only do that in a DM! The guild id you need is {}".format(
                    context.guild.id
                )
            )
        if isinstance(error, asyncio.TimeoutError):
            await context.send("Please react with 👍 or 👎", delete_after=5.0)
            return

    @commands.Cog.listener()
    @commands.guild_only()
    async def on_raw_reaction_add(self, payload):
        """Watches for :OK: on a message in REVIEW_CHANNEL_NAME, by a Mod.

        Then moves Suggestion to #suggestions channel and updates DB"""
        guild = self.bot.get_guild(payload.guild_id)
        channel = self.bot.get_channel(payload.channel_id)
        author = guild.get_member(payload.user_id)

        if str("🆗") != str(payload.emoji):
            return
        if isinstance(channel, discord.DMChannel):
            return
        if channel.name != REVIEW_CHANNEL_NAME:
            return
        if not any(role.name.lower() == "mods" for role in author.roles):
            await channel.send("Only mods can approve suggestions with 🆗!")
            return

        message = await channel.fetch_message(payload.message_id)
        embed = message.embeds[0]
        embed_dict = embed.to_dict()
        suggestion_id = int(embed_dict["fields"][2]["value"])

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(SUGGESTION_UPDATE, (suggestion_id,))
                await channel.send(
                    "{} approved suggestion #{}".format(author.mention, suggestion_id)
                )
        suggestion_channel = get(guild.text_channels, name=SUGGESTION_CHANNEL)
        embed.remove_field(2)
        await suggestion_channel.send(embed=embed)


def setup(bot):
    """Setup the Suggestions Plugin"""
    setup_check_postgres(bot, __file__)
    bot.add_cog(SuggestionPlugin(bot))
