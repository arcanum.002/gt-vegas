"""This module provides all the functionality for the `ModContactPlugin`.

Attributes
----------
SELECT_CONTACT: str
    Used for an `aiopg` execution. Returns the 24 most recent open Mod Contacts for the related user.
VIEW_CONTACT: str
    Used for an `aiopg` execution. Returns a single discord_user_discordmodcontact record and all related responses.
NEW_CONTACT: str
    Used for an `aiopg` execution. Adds a new record to discord_user_discordmodcontact.
MOD_SELECT_CONTACT: str
    Used for an `aiopg` execution. Returns the all open Mod Contacts for the related user.
MOD_REPLY_CONTACT: str
    Used for an `aiopg` execution. Returns the a Mod Contact and all Replies for one Contact.
CLOSE_CONTACT: str
    Used for an `aiopg` execution. Updates a record to be marked as `closed`.
INSERT_RESPONSE: str
    Used for an `aiopg` execution. Inserts a "reply" into the Database.
MOD_CONTACT_LIST: str
    SQL Query. Returns a list of all current open mod contacts for the server. Requires a Guild ID and Offset: int.
TIMEOUT: double
    The module-wide timeout for async/wait_for functions.
"""
import datetime
import asyncio

from typing import Union, List
from functools import partial

import discord
from discord import Member, User
from discord.ext import commands
from discord.utils import get
from discord import Embed
from discord.ext.commands import has_any_role

from settings.base import COLORS
from plugins.base import BasePlugin

from checks import requires_postgres
from checks import setup_check_postgres

COLOR = COLORS["ban"]

TIMEOUT = 30.0

SELECT_CONTACT = "SELECT c.id, c.date_recieved, c.reason, c.urgency, c.contact_back, c.discord_user_id, COUNT(r.id) responses, c.closed  FROM discord_user_discordmodcontact AS c LEFT OUTER JOIN discord_user_discordmodcontactresponses AS r ON r.contact_id=c.id WHERE c.discord_user_id=%s AND c.closed='f' GROUP BY c.id ORDER BY c.closed ASC LIMIT 24;"
VIEW_CONTACT = "SELECT * FROM discord_user_discordmodcontactresponses AS r RIGHT JOIN discord_user_discordmodcontact AS mc ON mc.id=r.contact_id WHERE mc.id=%s"
NEW_CONTACT = "INSERT INTO discord_user_discordmodcontact(date_recieved, reason, urgency, contact_back, discord_user_id, closed) VALUES(%s, %s, %s, %s, %s, 'f') RETURNING id;"
MOD_SELECT_CONTACT = "SELECT c.id, c.date_recieved, c.reason, c.urgency, c.contact_back, c.discord_user_id, COUNT(r.id) responses, c.closed, u.discord_user_id  FROM discord_user_discordmodcontact AS c LEFT OUTER JOIN discord_user_discordmodcontactresponses AS r ON r.contact_id=c.id LEFT OUTER JOIN discord_user_discorduser AS u ON u.id=c.discord_user_id WHERE c.closed='f' GROUP BY c.id,u.discord_user_id ORDER BY c.closed ASC;"
MOD_REPLY_CONTACT = "SELECT c.id, c.date_recieved, c.reason, c.urgency, c.contact_back, c.discord_user_id, COUNT(r.id) responses, c.closed, u.discord_user_id  FROM discord_user_discordmodcontact AS c LEFT OUTER JOIN discord_user_discordmodcontactresponses AS r ON r.contact_id=c.id LEFT OUTER JOIN discord_user_discorduser AS u ON u.id=c.discord_user_id WHERE c.closed='f' AND c.id=%s GROUP BY c.id,u.discord_user_id ORDER BY c.closed ASC;"
CLOSE_CONTACT = "UPDATE discord_user_discordmodcontact SET closed='t' WHERE id=%s;"
INSERT_RESPONSE = "INSERT INTO discord_user_discordmodcontactresponses(date, message, mod_response, contact_id) VALUES (%s, %s, %s, %s)"
MOD_CONTACT_LIST = "select mc.id, mc.date_recieved, mc.reason, mc.urgency, mc.contact_back, COUNT(r.id) responses, u.discord_user_id from discord_user_discordmodcontact as mc INNER JOIN discord_user_discorduser AS u ON u.id=mc.discord_user_id LEFT JOIN discord_user_discordmodcontactresponses as r ON r.contact_id=mc.id WHERE u.discord_guild_id=%s AND mc.closed='f' GROUP BY mc.id, u.discord_user_id ORDER BY mc.id DESC OFFSET %s ROWS FETCH NEXT 25 ROWS ONLY;"


class ModContactPlugin(BasePlugin):
    """Plugin to handle group communication between Mods and other users.

    Note
    ----
    This module will only function if you have a running Postgres installation
    and the gtdiscord.discord_user_discorduser* tables imported! If you need this
    check the `sql/` folder!
    """

    def __init__(self, bot):
        """Basic plugin init

        Parameters
        ----------
        bot: discord.ext.commands.Bot
        """
        self.bot = bot

    async def _new_contact(
        self, caller: Union[User, Member], guild: discord.Guild, link_id: str
    ):
        """Private method that walks the Caller through submiting a Mod Report.

        This method has three major sections.

        The first section is wrapped in a `try` block and contains the logic
        the bot uses to collect the Callers report. Each question is self contained
        in a `while True:` loop. These loops contain the validation for each question
        and a break condition.

        The second section build an embed with the new information, and displays the embed
        to the Caller for confirmation.

        Once confirmed the bot saves this new report using `NEW_CONTACT` and posts the confirmation
        embed to a #mod-contact channel on the appropriate guild.

        Parameters
        ----------
        context: discord.Context
            A discordpy Context object.

        guild: discord.Guild
            The Guild that this report is for.

        link_id: int
            The discord_user_discorduser id for the Callers record.

        Note
        ----
        - Uses `NEW_CONTACT`
        """
        await caller.send(
            "OK! We take all reports very seriously! Let me start a new report now. I will just need to ask a few questions..."
        )

        try:
            while True:
                # determines how "urgent" the issue is and validates the reply is a number
                await caller.send(
                    "On a scale of 1 to 7, how urgent is this issue (1 - Not urgent, 7 - Incredibly urgent)"
                )

                # Using a partial we can insert the value we wan to check for later now.
                check_is_dm_and_author_message = partial(
                    self._check_is_dm_and_author_message_partial, caller
                )

                urgency = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )

                try:
                    urgency = int(float(urgency.content))
                    if urgency in range(1, 8):
                        break
                    else:
                        await caller.send("Please provide a value between 1 - 7")
                except ValueError:
                    await caller.send("Please provide a value between 1 - 7")

            while True:
                # Collects the Callers response
                await caller.send(
                    "Please tell us about your issue in detail. If you would like to supply "
                    "screenshots or images, please use Imgur or other filesharing platforms "
                    "and provide URLs."
                )
                reason = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )

                if len(reason.content) < 950:
                    break

                await caller.send(
                    "Sorry! Due to Discord limitions, please keep replies under 950 "
                    "characters. Include links to a google doc or images if needed!"
                )

            while True:
                # determines if the caller would like contacted back and validates the reply is a number
                await caller.send("Would you like a mod to contact you? (y/n)")
                contact = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
                if contact.content.lower() in [
                    "y",
                    "yes",
                    "ye",
                ]:
                    contact = True
                    contact_back = "Yes"
                    break
                elif contact.content.lower() in [
                    "n",
                    "no",
                ]:
                    contact = False
                    contact_back = "No thank you"
                    break
                else:
                    await caller.send("Please answer 'yes' or 'no'.")

        except asyncio.TimeoutError:
            await caller.send(
                "Your Mod Contact session has timed-out. {} will wait "
                "40 seconds before timing out each question.".format(self.bot.name)
            )

        # Builds and sends the confimration embed
        desc = "Please take a moment to review your contact! This will be submitted to the Server when you're finished."
        embed = Embed(title="New Mod Contact", description=desc, color=COLOR)
        embed.add_field(name="Urgency:", value=urgency)
        embed.add_field(name="Contact Back:", value=contact_back)
        embed.add_field(
            name="Reporter:", value="{} - ID: {}".format(caller.name, caller.id)
        )
        embed.add_field(name="Reason:", value=reason.content, inline=False)
        embed.set_author(name=caller.name, icon_url=caller.avatar_url)
        embed.set_footer(
            text="If this is correct, react with 👍 to submit. Otherwise, red ❌ to close and clear."
        )

        embed_message = await caller.send(embed=embed)

        try:

            check_is_dm_and_author_reaction = partial(
                self._check_is_dm_and_author_reaction_partial, caller
            )

            while True:
                reaction, reaction_user = await self.bot.wait_for(
                    "reaction_add",
                    timeout=TIMEOUT,
                    check=check_is_dm_and_author_reaction,
                )
                if reaction.emoji == "👍":
                    async with self.bot.pg_pool.acquire() as conn:
                        async with conn.cursor() as cursor:
                            # Creates the new record
                            await cursor.execute(
                                NEW_CONTACT,
                                (
                                    datetime.datetime.now(),
                                    reason.content,
                                    urgency,
                                    contact,
                                    link_id,
                                ),
                            )
                            await cursor.execute(SELECT_CONTACT, (link_id,))
                            mid = await cursor.fetchall()
                            mid = mid[-1][0]
                            await caller.send(
                                "Thanks for the mod contact! Your contact id is {}".format(
                                    mid
                                )
                            )
                            # Sends the confirmation embed to the #mod-contacts channel and pins
                            mod_channel = get(guild.text_channels, name="mod-contacts")
                            embed.add_field(name="Contact ID:", value=mid, inline=False)
                            mod_message = await mod_channel.send(embed=embed)
                            await mod_message.pin()
                            return
                elif reaction.emoji == "❌":
                    await embed_message.edit(
                        content="Sorry to hear that. If you have any other issues, please report them right away!",
                        embed=None,
                    )
                    return
                else:
                    # await embed_message.clear_reactions()
                    warning = await caller.send("Please react with 👍 or red ❌")
                    await warning.delete(delay=5)
        except asyncio.TimeoutError:
            await caller.send("You're session has been closed due to inactivity!")
            return

    async def _view_contact(self, caller: Union[Member, User], guild: discord.Guild):
        """Private method that displays all replies for a specific Mod Report.

        Parameters
        ----------
        context: discord.Context
            A discordpy Context object.
        guild: discord.Guild
            The guild this mod_contact will be associated with.
        link_id: str
            The id of this users discord_user_discorduser record.
        """

        check_is_dm_and_author_message = partial(
            self._check_is_dm_and_author_message_partial, caller
        )

        while True:
            try:
                await caller.send("What contact do you want to view? Reply with an ID.")
                contact_id = await self.bot.wait_for(
                    "message", timeout=TIMEOUT, check=check_is_dm_and_author_message
                )
            except asyncio.TimeoutError:
                await caller.send("Timeout")

            if contact_id.content.isdigit():
                contact_id = int(contact_id.content)
                break

        await self._view_single_contact(guild, caller, contact_id, caller)

    async def _view_single_contact(
        self,
        guild: discord.Guild,
        caller: Union[Member, User],
        contact_id: int,
        send_to: Union[Member, User],
    ):
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                # Creates the new record
                await cursor.execute(VIEW_CONTACT, (contact_id,))
                contacts = await cursor.fetchall()
                await cursor.execute(
                    "SELECT * FROM discord_user_discorduser WHERE id=%s",
                    (contacts[0][10],),
                )
                member_id = await cursor.fetchone()

        # Ensure only mods and the submitter can view contacts
        if isinstance(caller, Member):
            # Ensures that the caller is a mod or in a DM channel
            if not any(role.name.lower() == "mods" for role in caller.roles):
                await send_to.send(
                    "You cannot view other users mod contact reports if you are not a mod."
                )
                return

        elif not (caller.id == member_id[2]):
            await send_to.send("You cannot view other users mod contact reports.")
            return

        contact_author = self.bot.get_user(member_id[2])
        is_open = "Closed" if bool(contacts[0][11]) else "Open"
        fmt_date = contacts[0][6].strftime("%B %d %Y")
        title = "{} - ID: {} - {}".format(is_open, contacts[0][5], fmt_date)

        embed = Embed(title=title, description=contacts[0][7], color=COLOR)
        embed.set_author(name=contact_author.name, icon_url=contact_author.avatar_url)

        if contacts[0][1]:
            for contact in contacts:
                reply_author = "Mod" if bool(contact[3]) else contact_author.name
                reply_date = contact[1].strftime("%B %d %Y")
                field_name = "{} replied on {}".format(reply_author, reply_date)
                embed.add_field(name=field_name, value=contact[2], inline=False)

        embed.set_footer(
            text="You can reply with `v!mod_contact reply {} Your reply here` | Server ID: {}".format(
                contacts[0][5], guild.id
            )
        )

        await send_to.send(embed=embed)

    @commands.group(description="Mod reports for this server.", case_insensitive=True)
    @requires_postgres()
    async def mod_contact(self, context):
        """Placeholder for the ModContact Group command."""
        if context.invoked_subcommand is None:
            caller = self.bot.get_user(context.message.author.id)
            guild = await self._get_guild(context)
            if guild is None:
                return

            link_id = await self._check_user_exists(caller.id, guild.id)

            # Build Embed
            embed_title = "Welcome to {bot_name} Mod Contact!".format(
                bot_name=self.bot.user.name
            )
            description = (
                "This embed displays all Mod Contact Reports from {author} to "
                "the Mod Team on {server_name}. You can view all reports you have submitted."
            )
            description = description.format(author=caller.name, server_name=guild.name)

            embed = Embed(title=embed_title, description=description, color=COLOR)
            embed.set_author(name=caller.name, icon_url=caller.avatar_url)
            embed.set_thumbnail(url=guild.icon_url)
            embed.set_footer(
                text="React with ➕ to submit a new report, 💬 to reply or view mod replies, or ❌ to close."
            )

            # Get mod contacts and add fields
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(SELECT_CONTACT, (link_id,))
                    contacts = await cursor.fetchall()

            # Attach Server Data as a field
            server_val = "{}".format(guild.name)
            if guild.description:
                server_val = "{}\n{}".format(guild.name, guild.description)

            embed.add_field(name="Server:", value=server_val)
            if contacts:
                # Attach total reports as a field
                embed.add_field(name="Total Reports:", value=len(contacts))

                for contact in contacts:
                    # Loop through each report and attach as a field
                    is_open = "Closed" if bool(contact[7]) else "Open"
                    field_name = "{is_open} - ID : {cid} - {date}".format(
                        is_open=is_open,
                        cid=contact[0],
                        date=contact[1].strftime("%B %d %Y"),
                    )
                    field_val = (
                        "*Urgency:* {urg}\n"
                        "*Requested Contact Back:* {contact_back}\n"
                        "*Replies:* {replies}\n"
                        "*Reason:* {reason}".format(
                            urg=contact[3],
                            contact_back=contact[4],
                            replies=contact[6],
                            reason=contact[2],
                        )
                    )
                    embed.add_field(name=field_name, value=field_val, inline=False)

            await caller.send(embed=embed)

            check_is_dm_and_author_reaction = partial(
                self._check_is_dm_and_author_reaction_partial, caller
            )

            while True:
                try:
                    reaction, reaction_user = await self.bot.wait_for(
                        "reaction_add",
                        timeout=TIMEOUT,
                        check=check_is_dm_and_author_reaction,
                    )
                    if reaction.emoji == "➕":
                        await self._new_contact(caller, guild, link_id)
                        break
                    elif reaction.emoji == "💬":
                        await self._view_contact(caller, guild)
                        break
                    elif reaction.emoji == "❌":
                        await caller.send("Canceling...")
                        break
                except asyncio.TimeoutError:
                    warning = await caller.send(
                        "If you would like to cancel, react to the embed with :x:."
                    )
                    await warning.delete(delay=3)

    @mod_contact.command(
        description="View mod contact replies",
        case_insensitive=True,
        aliases=[
            "v",
        ],
    )
    @commands.guild_only()
    @has_any_role("Mods")
    @requires_postgres()
    async def view(self, context, contact_id: int):
        if not contact_id:
            await context.send("I need a number to lookup.")
            return

        await self._view_single_contact(
            context.guild, context.message.author, contact_id, context.channel
        )

    @mod_contact.command(
        description="Close a Mod Contact Report. (Mod)",
        case_insensitive=True,
        aliases=[
            "c",
        ],
    )
    @commands.guild_only()
    @has_any_role("Mods")
    @requires_postgres()
    async def close(self, context, contact_id=None):
        """Closes a Mod Contact.

        Parameters
        ----------
        contact_id: int or None
            The id for a discord_user_discordmodcontact record.

        Note
        ----
        - Uses `CLOSE_CONTACT`
        """

        # Saftey validation that the contact_id is a number
        if not contact_id or not contact_id.isdigit():
            await context.send("Please provide a contact id (number).")
            return

        async with context.channel.typing():
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(CLOSE_CONTACT, (contact_id,))

        await context.send("{} has been closed!".format(contact_id))

    @mod_contact.command(
        description="List open mod contacts",
        case_insensitive=True,
        aliases=[
            "l",
        ],
    )
    @commands.guild_only()
    @has_any_role("Mods")
    @requires_postgres()
    async def list(self, context, page=1):
        guild = context.guild
        offset = (page - 1) * 25
        async with context.channel.typing():
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(MOD_CONTACT_LIST, (guild.id, offset))
                    open_contacts = await cursor.fetchall()
        desc = "Current open Mod Contacts."
        embed = discord.Embed(
            title="Mod Contact Page: {}".format(page),
            description=desc,
            color=context.message.author.top_role.color,
        )
        embed.set_thumbnail(url=guild.icon_url)
        for contact in open_contacts:
            mcid = contact[0]
            recieved = contact[1]
            reason = contact[2]
            urgency = contact[3]
            contact_back = contact[4]
            responses = contact[5]
            user_id = contact[6]
            user = get(context.guild.members, id=user_id)
            if not user:
                user = object()
                user.id = user_id
                no_user_fmt = "<{}> [ No longer in server ]".format(user.id)
                user.username = no_user_fmt
                user.name = user.username
                user.mention = user.username
            field_name = "{name} - ID: {mcid} - {date}".format(
                name=user.name, mcid=mcid, date=recieved.strftime("%Y-%m-%d")
            )
            field_value_fmt = (
                "User: {mention}  -  User ID: {uid}\n"
                "*Urgency*: {urgency}\n*Contact Back*: {contact_back}\n"
                "Responses: {responses}\n**Reason**: {reason}"
            )
            field_value = field_value_fmt.format(
                mention=user.mention,
                uid=user.id,
                urgency=urgency,
                contact_back=contact_back,
                responses=responses,
                reason=reason,
            )
            embed.add_field(name=field_name, value=field_value, inline=False)
        await context.send(embed=embed)

    @mod_contact.command(
        description="Reply to a Mod Contact Report. (Mod)",
        case_insensitive=True,
        aliases=[
            "r",
        ],
    )
    @requires_postgres()
    async def reply(self, context, contact_id: int, *reply: List[str]):
        """Used to reply to a users Open Mod Contact.

        Parameters
        ----------
        contact_id: int
            The ID for the Mod Contact Record to reply to.
        *reply: list(str)
            A list of words to use as the Reply.

        Note
        ----
        - Uses _build_single_embed_contact
        """
        now = datetime.datetime.now()
        member = context.message.author
        reply = " ".join(reply)
        guild = await self._get_guild(context)
        if guild is None:
            return

        if len(reply) > 950:
            await context.send(
                "Sorry! Due to Discord limitions, please keep replies under 950 "
                "characters. Include links to a google doc or images if needed!"
            )
            return

        reply_output = "**{date}**\n{reason}".format(
            date=now.strftime("%B %d %Y"), reason=reply
        )

        # Fetch contact information
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(MOD_REPLY_CONTACT, (contact_id,))
                contact_data = await cursor.fetchone()
        if contact_data is None:
            await member.send("Sorry! I couldn't find an open contact with that ID.")
            return
        contact_member = discord.utils.get(guild.members, id=contact_data[8])

        """Checks if the user is a moderator and if the user can Reply."""
        is_mod = False
        can_reply = False

        # is the Mod Contact owner and the Caller the same?
        if contact_member.id == member.id:
            can_reply = True
        if isinstance(member, Member):
            # is the Caller a Mod?
            if any("Mods" == role.name for role in member.roles):
                can_reply = True
                is_mod = True

        # Safety validation, checks that contact_id is a number
        if not contact_id and contact_id.isdigit():
            await context.send("Please send a contact id (number).")
            return

        # Fall back if user cannot reply
        if not can_reply:
            await context.send("You cannot reply to this Mod Contact.")
            return

        embed_title = "{username} Report - {date}".format(
            username=member.name, date=contact_data[1].strftime("%B %d %Y")
        )
        desc = contact_data[2]
        contact_back = "Yes" if contact_data[4] else "No thank you"
        contact_reply_embed = Embed(title=embed_title, description=desc, color=COLOR)
        contact_reply_embed.add_field(name="Urgency:", value=contact_data[3])
        contact_reply_embed.add_field(name="Contact Back:", value=contact_back)
        contact_reply_embed.add_field(name="Replies:", value=contact_data[6])

        await context.send(embed=contact_reply_embed)
        await context.send(
            "The following reply will be added to Contact {}".format(contact_id)
        )
        await context.send(reply_output)
        await context.send(
            "Is this alright? React with :thumbsup: to send :x: to cancel."
        )

        def is_contact_and_dm_or_mod(emoji, user):
            """on_reaction_add check method to ensure Reactor is Caller and is in DMChannel"""
            return (member == user) and (
                isinstance(emoji.message.channel, discord.DMChannel)
                or any(role.name.lower() == "mods" for role in member.roles)
            )

        while True:
            try:
                # Setup seperate checks
                reaction, _ = await self.bot.wait_for(
                    "reaction_add", timeout=TIMEOUT, check=is_contact_and_dm_or_mod
                )
                if reaction.emoji == "👍":
                    # If everything is correct, save to DB
                    async with self.bot.pg_pool.acquire() as conn:
                        async with conn.cursor() as cursor:
                            await cursor.execute(
                                INSERT_RESPONSE,
                                (datetime.datetime.now(), reply, is_mod, contact_id),
                            )
                            await context.send("Reply added!")
                            break

                elif reaction.emoji == "❌":
                    # Canceling requires nothing!
                    await context.send("Okay! Canceling!")
                    break

            except asyncio.TimeoutError:
                warning = await context.send("If you want to cancel react with :x:")
                await warning.delete(delay=5)

        if is_mod:
            await contact_member.send(
                "New reply for your Mod Contact ID: {}! \n\n{}".format(
                    contact_id, reply
                )
            )

        else:
            mod_channel = get(guild.text_channels, name="mod-contacts")
            await mod_channel.send("New Mod Contact Reply on ID: {}".format(contact_id))


def setup(bot):
    """Setup function for Cogs"""
    setup_check_postgres(bot, __file__)
    bot.add_cog(ModContactPlugin(bot))
