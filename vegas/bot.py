import datetime
import sys
import traceback
import asyncio

from discord import Intents
from discord import Forbidden
from discord import Game
from discord.ext.commands import Bot
from discord.ext.commands.errors import MissingAnyRole

from settings.base import POSTGRES_ERR
from settings.base import REDIS_ERR
from settings.local_settings import BOT_PREFIX
from settings.local_settings import COGS
from settings.local_settings import MAX_MESSAGES
from settings.local_settings import setup_redis
from settings.local_settings import setup_postgres_pool
from settings.local_settings import DEBUG
from exceptions import PostgresNotInstalled, RedisNotInstalled


class VEGAS(Bot):
    """Base VEGAS bot"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_time = datetime.datetime.now()

        self.loop.run_until_complete(setup_redis(self))
        self.loop.run_until_complete(setup_postgres_pool(self))

        # Gets the 'plugins' directory
        for cog in COGS:
            try:
                self.load_extension(cog)
            except Exception:
                print(f"Failed to load extension {cog}.", file=sys.stderr)
                traceback.print_exc()

    async def on_ready(self):
        """Default startup command"""
        print("Bot on_ready()")
        await client.change_presence(activity=Game(name="with development"))
        print("Logged in as " + client.user.name)

    async def on_error(self, event, ctx, error, **kwargs):
        """Default Error Handling for whole bot.
        If your method/cog needs a custom error message,
           create a custom Exception, and match against it here!"""
        if hasattr(ctx.command, "on_error"):
            return
        if isinstance(error, MissingAnyRole):
            await ctx.send("You don't have the roles to do that!")
        elif isinstance(error, PostgresNotInstalled):
            err = f"{ctx.cog.qualified_name}: {POSTGRES_ERR}"
            if DEBUG:
                print(err)
            else:
                ctx.guild.owner.send(err)
            await ctx.send(err)
        elif isinstance(error, RedisNotInstalled):
            err = f"{ctx.cog.qualified_name}: {REDIS_ERR}"
            if DEBUG:
                print(err)
            else:
                ctx.guild.owner.send(err)
            await ctx.send(err)
        else:
            print(error)

    async def dm_author(self, ctx, message):
        """Helper method to direct message the user that sent the message"""
        try:
            await self.send_message(ctx.message.author, message)

        except Forbidden:
            await self.send_message(message)

    async def on_command_error(self, event, context, error):
        print(error)


intents = Intents.default()  # All but the two privileged ones
intents.members = True  # Subscribe to the Members intent

if sys.platform == "win32":
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

client = VEGAS(
    messages=MAX_MESSAGES,
    command_prefix=BOT_PREFIX,
    case_insensitive=True,
    intents=intents,
)
