--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: bot_forms_botform; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bot_forms_botform (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    link character varying(200) NOT NULL
);


ALTER TABLE public.bot_forms_botform OWNER TO postgres;

--
-- Name: bot_forms_botforms_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bot_forms_botforms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bot_forms_botforms_id_seq OWNER TO postgres;

--
-- Name: bot_forms_botforms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bot_forms_botforms_id_seq OWNED BY public.bot_forms_botform.id;


--
-- Name: bot_forms_botform id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bot_forms_botform ALTER COLUMN id SET DEFAULT nextval('public.bot_forms_botforms_id_seq'::regclass);


--
-- Name: bot_forms_botform bot_forms_botforms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bot_forms_botform
    ADD CONSTRAINT bot_forms_botforms_pkey PRIMARY KEY (id);


--
-- Name: TABLE bot_forms_botform; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.bot_forms_botform TO user};


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: bot_forms_botformresult; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bot_forms_botformresult (
    id integer NOT NULL,
    discord_user_id character varying(255) NOT NULL,
    form_id integer NOT NULL
);


ALTER TABLE public.bot_forms_botformresult OWNER TO postgres;

--
-- Name: bot_forms_botformresults_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bot_forms_botformresults_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bot_forms_botformresults_id_seq OWNER TO postgres;

--
-- Name: bot_forms_botformresults_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bot_forms_botformresults_id_seq OWNED BY public.bot_forms_botformresult.id;


--
-- Name: bot_forms_botformresult id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bot_forms_botformresult ALTER COLUMN id SET DEFAULT nextval('public.bot_forms_botformresults_id_seq'::regclass);


--
-- Name: bot_forms_botformresult bot_forms_botformresults_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bot_forms_botformresult
    ADD CONSTRAINT bot_forms_botformresults_pkey PRIMARY KEY (id);


--
-- Name: bot_forms_botformresult_form_id_2c590ebc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX bot_forms_botformresult_form_id_2c590ebc ON public.bot_forms_botformresult USING btree (form_id);


--
-- Name: bot_forms_botformresult bot_forms_botformres_form_id_2c590ebc_fk_bot_forms; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bot_forms_botformresult
    ADD CONSTRAINT bot_forms_botformres_form_id_2c590ebc_fk_bot_forms FOREIGN KEY (form_id) REFERENCES public.bot_forms_botform(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE bot_forms_botformresult; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.bot_forms_botformresult TO user;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: bot_forms_discordformpage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bot_forms_discordformpage (
    page_ptr_id integer NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.bot_forms_discordformpage OWNER TO postgres;

--
-- Name: bot_forms_discordformpage bot_forms_discordformpage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bot_forms_discordformpage
    ADD CONSTRAINT bot_forms_discordformpage_pkey PRIMARY KEY (page_ptr_id);


--
-- Name: bot_forms_discordformpage bot_forms_discordfor_page_ptr_id_3794c866_fk_wagtailco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bot_forms_discordformpage
    ADD CONSTRAINT bot_forms_discordfor_page_ptr_id_3794c866_fk_wagtailco FOREIGN KEY (page_ptr_id) REFERENCES public.wagtailcore_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE bot_forms_discordformpage; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.bot_forms_discordformpage TO user;


--
-- PostgreSQL database dump complete
--

